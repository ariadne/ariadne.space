---
title: About
menu:
  main:
    title: 'About'
---

Friendly greetings!

I'm Ariadne Conill, the operator of this website.

I have spent the majority of my life hacking on [free software][fs].
My work includes starting [the IRCv3 project][ircv3], writing a lot
of the code commonly used on IRC servers, writing [Audacious][aud]
(a popular music player for Unix-like systems), [libucontext][luc],
[pkgconf][pc], [ifupdown-ng][ing] and several other key components
of the [Alpine Linux system][al].  Additionally, I have contributed
numerous security, robustness and privacy fixes to federated
software such as Mastodon and Pleroma, and standards such as
ActivityPub.  In a past life, I was a Debian contributor, but have
been working on Alpine exclusively since 2010.

   [fs]: https://en.wikipedia.org/wiki/Free_software_movement
   [ircv3]: https://ircv3.org/
   [aud]: https://audacious-media-player.org/
   [luc]: https://gitea.treehouse.systems/ariadne/libucontext
   [pc]: https://gitea.treehouse.systems/ariadne/pkgconf
   [ing]: https://github.com/ifupdown-ng/ifupdown-ng
   [al]: https://alpinelinux.org/

Presently, I am a principal engineer at [Chainguard][cg], focusing
on improving container base images and software distribution
ecosystems.

   [cg]: https://chainguard.dev/

If you feel like your project or organization could benefit from
my insight, feel free to drop me a line.

## Contact

I'm `Ariadne` on most IRC networks.  Until May 22, 2021, this
included freenode, but anybody on freenode past this date using
that nickname is likely an impersonator.

You can also email me at ariadne -at- ariadne.space.

I also founded a community of like-minded folks that presently
use [Discord][dt].  Please read the rules before participating.

   [dt]: https://treehouse.systems/discord
