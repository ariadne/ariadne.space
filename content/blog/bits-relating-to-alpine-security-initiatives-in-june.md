---
title: "Bits relating to Alpine security initiatives in June"
date: "2021-07-01"
---

As usual, I have been hard at work on various security initiatives in Alpine the past month.  Here is what I have been up to:

## Alpine 3.14 release and remediation efforts in general

Alpine 3.14.0 was released on June 15, with the lowest unpatched vulnerability count of any release in the past several years.  While previous Alpine release cycles did well on patching the critical vulnerabilities, the less important ones frequently slipped through the cracks, due to the project being unable to focus on vulnerability remediation until now.

We have also largely cleaned up Alpine 3.13 (there are a few minor vulnerabilities that have not been remediated there yet, as they require ABI changes or careful backporting), and Alpine 3.12 and 3.11 are starting to catch up in terms of unpatched vulnerabilities.

While a release branch will realistically never have zero unpatched vulnerabilities, we are much closer than ever before to having the supported repositories in as optimal of a state as we can have them.  Depending on how things play out, this may result in extended security support for the `community` repository for 3.14, since the introduction of tools and processes has reduced the maintenance burden for security updates.

Finally, with the release of Alpine 3.14, the security support period for Alpine 3.10 draws to a close, so you should upgrade to at least Alpine 3.11 to continue receiving security updates.

## secfixes-tracker and the security database

This month saw a minor update to `secfixes-tracker`, the application which powers [security.alpinelinux.org](https://security.alpinelinux.org/).  This update primarily focused around supporting the new [security rejections database](https://gitlab.alpinelinux.org/kaniini/security-rejections), which allows for maintainers to reject CVEs from their package with an annotated rationale.

In my [previous update](https://ariadne.space/2021/06/04/a-slightly-delayed-monthly-status-update/), I talked about a proposal which will allow [security trackers to exchange data, using Linked Data Notifications](https://docs.google.com/document/d/11-m_aXnrySM6KeA5I6BjdeGeSIxymfip4hseg2Y0UKw/edit#heading=h.bz0hbmpvjhfb).  This will be deployed on security.alpinelinux.org as part of the `secfixes-tracker` 0.4 release, as we have come to an agreement with the Go and OSV teams about how to handle JSON-LD extensions in the format.

My goal with the Linked Data Notifications effort is to decentralize the current CVE ecosystem, and a bit longer writeup explaining how we will achieve that is roughly half-way done sitting around in my drafts folder.  Stay tuned!

Finally, the [license for the security database has been officially defined as CC-BY-SA](https://gitlab.alpinelinux.org/alpine/infra/docker/secdb/-/commit/0a7e5cb403c0ca097ff1b9a45dd942e4a23356a3), meaning that security vendors can now use our security database in their scanners without having a legal compliance headache.

## Reproducible Builds

We have begun work on supporting reproducibility in Alpine.  While there is still a lot of work to be done in `abuild` to support buildinfo files, kpcyrd started [to work on making the install media reproducible](https://twitter.com/sn0int/status/1408853977106718724), beginning with the Raspberry Pi images we ship.

However, he ran into an issue with BusyBox's `cpio` not supporting reproducibility, so I added the necessary flags to allow for cpio archives to be reproducible, [sent the patches to upstream BusyBox](http://lists.busybox.net/pipermail/busybox/2021-June/088953.html) and pushed an updated BusyBox with the patches to Alpine edge.

There are still a few fixes that need to be made to `apk`, but with some workarounds, we were able to [demonstrate reproducible install images for the Raspberry Pi](https://twitter.com/sn0int/status/1410280462296268809).

The next few steps here will involve validating the reproducible initramfs work correctly, for example I don't think we need `--ignore-devno`, just `--renumber-inodes` for it, and I also think that with `--ignore-devno` it won't actually boot, but validation will allow us to verify everything is OK with the image.

Beyond that, we need reproducible packages, and for that, we need buildinfo files.  That's next on my list of things to tackle.

## The `linux-distros` list

In the [last update](https://ariadne.space/2021/06/04/a-slightly-delayed-monthly-status-update/), we were discussing whether to join the `linux-distros` list.  Since then, we concluded that joining the list does not net us anything useful: our post-embargo patching timeframe is the same as distros which participate on the list, and the requirements for sharing vulnerability data with other team members and maintainers were too onerous.  Alpine values transparency, we found that compromising transparency to have embargoed security data was not a useful tradeoff for us.

## apk-tools 3

Since the last update, Timo has made a lot of progress on the ADB format used in apk-tools 3.  At this point, I think it has come along enough that we can begin working on exposing security information the ADB-based package indices.

While Alpine itself is not yet publishing ADB-based indices, the features available in the ADB format are required to reflect the security fix information correctly (the current index format does not support structured data at all, and is just a simple key-value store).

I also intend to look at the ADB-based indices to ensure they are reproducible.  This will likely occur within the next few weeks as I work on making the current indices reproducible.

## Acknowledgement

My activities relating to Alpine security work are presently sponsored by Google and the Linux Foundation. Without their support, I would not be able to work on security full time in Alpine, so thanks!
