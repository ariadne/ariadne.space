---
title: "So you've decided to start a free software consultancy..."
date: '2022-08-11'
---

Recently a friend of mine told me that he was planning to start a
free software consultancy, and asked for my advice, as I have an
extensive background doing free software consulting for a living.
While I have already given him some advice on how to proceed, I
thought it might be nice to write a blog expanding on my answer,
so that others who are interested in pursuing free software
consulting may benefit.

## Framing the value proposition

There are many things to consider when launching a free software
consultancy, but the key aspect to consider is how you frame the
value proposition of your consultancy.  A common mistake that new
founders make when starting their free software consultancies is
to frame the value proposition toward developers.  Rather than
doing this, you should frame your value proposition towards
management.

For example, my friend described the value proposition of his
consultancy like this:

"I help people manage their open source server stuff for money."

This is not a good way to frame the value proposition of a
consultancy, because the manager will inevitably ask a question
like:

"Why can't we just hire an intern to manage that?"

In this case, the manager is right to ask a question like that,
because the value proposition is not correctly framed.
The purpose of a free software consultancy is to *augment* the
business' IT competencies by leveraging the consultant's gained
experience working in FOSS.  When you frame your value proposition
this way, it becomes more clear to the management why they
should engage with your consultancy.

When pitching your value proposition to a prospective client,
you should try to empathise with the needs of the client, and
tailor your value proposition around how your consultancy can
satisfy their needs.

## Pricing for services

For serious engagements, pricing should be defined as a function
of the value gained for the client from the engagement.  For
example, if the client saves $250k as a result of the engagement,
then you should charge a percentage of that savings.

Proof of concept engagements should be priced lower than your
standard rate, as they represent higher risk for the client.
Since they are priced lower, the scope of work should also be
reduced verses a normal engagement.  A common strategy is to
split proof of concept engagements into phases, so that the
client does not have to commit budget for the entire engagement
up front, which can provide an opportunity to charge a little
more for the overall engagement.

When you are starting out, you will also want to focus on
recurring revenue.  This provides two key benefits: first,
you have a bottom line greater than $0 if you aren't able
to close larger engagements, which happens from time to time,
especially during the summer months, as managers tend to go
on holiday.  Second, the recurring revenue customers, assuming
that you provide them with good service, will recommend your
consultancy to others, including large businesses.

These types of engagements should be priced according to what
you believe to be a fair value for X hours of your time per
month.  As a general rule, most consultancies charge at least
$100 per hour for prepaid consultancy services.

An example of a recurring service would be something like
server maintenance for a small business.  In this case, you
are augmenting the business with IT services, but the engagement
is likely to not require a large amount of time, meaning that
with automation, you can build out a customer portfolio of a
few hundred of these engagements.

## Professional services networks

It is critical to pursue certifications like the RHCE.  The
value in these certifications are the access to the professional
services networks they provide.  They will also help with
customers who have compliance requirements that state that
the engineers working on a project have to be certified.

Larger firms like Red Hat largely outsource their professional
services engagements to consultancies which have passed their
certification and joined their partner network.  These types of
relationships are critical: you get to leverage the power of
the larger firm's sales capability to acquire new engagements
by bidding on them.

Similarly, you should seek out partnerships with other
consultancies, as doing so will expand the range of capabilities
that your consultancy has.  For example, you might not have
familiarity with enterprise networking equipment, but if you have
a relationship with a consultancy that does have the ability to
take on managing enterprise networking equipment, then you can
join forces and bid on contracts which have that requirement
in their success criteria.

All of the companies from Red Hat to AWS have professional
services networks.  Find the ones relevant to the skills your
consultancy has and join them.

## Invoicing and payment

Larger engagements will **always** be NET-30 at the least, where
NET means no earlier than X days.  This allows the client the
ability to check your work and ensure they are satisfied with
what you have delivered.

If you need the money sooner, there are a few options.  First
you can offer a discount for paying early, an industry standard
is a 10% early payment discount.  Another option is to use a
factoring company.  Factoring works by selling the obligation
to a third party, which collects on your behalf for a fee and
advances you the payment.  If you use a payment platform such
as Quickbooks or Bill.com, these platforms have integration with
factoring companies, allowing you to get payment sooner.

## Negotiation

An engagement will always consist of a written contract with a
Statement of Work, frequently called an SOW.  The SOW lays out
the success criteria for the engagement.  SOWs can be open-ended
or they can be highly precise.  There are advantages to both
approaches when authoring an SOW, but an open-ended SOW can wind
up creating problems during the engagement, as it provides
flexibility for both you *and* your client.

Always negotiate deals in writing, never take an engagement on
an oral promise alone.  If a deal requires a third-party to provide
some of the success criteria, get their commitment in writing,
or you may be left holding the bag.

## Following up

An engagement should ideally be thought of as a free-flowing
conversation that results in the resolution of the success
criteria stated in the SOW.  Accordingly, it is *vital* to
keep the conversation going.

This means that you should follow up with the client on a
regular basis to keep them informed of the progress of the
work being done as part of the engagement, and to solicit
feedback early.  It is far easier to change the course of
an engagement earlier than after hundreds of hours have
gone into the work.

When discussing the engagement, it should be considered an
active listening exercise: you lay out what your team is
building, and then the client provides feedback based on
your presentation.  From there, the conversation moves into
defining what forward progress looks like.

## Takeaways

These are just my observations from nearly 20 years of doing
professional consulting around FOSS.  There is no singular
right way of running a consultancy, but these are the key
aspects that helped me to maintain good working relationships
with my customers.

Running a FOSS consultancy is hard work, but can result in
a sustainable business, if you are willing to put in the
work.
