---
title: "The internet is broken due to structural injustice"
date: "2022-10-27"
---

Over the past few years, I've come to realize that the Internet as we know
it is utterly broken.  Lately, I've also been pondering how participants
in the modern Internet have enabled and perpetuated harm to society at
large.  Repeatedly, we have seen the independence of the commons chipped
away by powerful men who wish for participants to serve their own whims,
while those who raise concerns with these developments are either shunned,
banned or doxed.

On Friday, October 28th, we will see another demonstration of these structural
injustices where the commons takes another loss to the whims of a powerful man.
Last time, [it was freenode's takeover by Andrew Lee][fn], and this time it
will be Twitter's takeover by Elon Musk.  No, really, the deal is already
concluded: [TWTR will be delisted from NASDAQ on Friday][twtr-delisting].

   [fn]: https://ariadne.space/2021/05/20/the-whole-freenode-kerfluffle/
   [twtr-delisting]: https://seekingalpha.com/news/3896099-twitter-delisting-from-nyse-effective-on-friday-after-musk-completes-deal

Will this be the end of Twitter?  Probably not, but it will be the end of the
current relationship the commons shares with Twitter.  Instead of acting as
a self-described "public square," it will further evolve into a chaotic
cacophony of trolling and counter-trolling driven in the name of algorithmic
engagement.  Some will move to other microblogging services and networks,
and will likely discover that everything which made Twitter horrible likely
applies in some way to the replacement.

## Are social platforms working as designed?

The reality is that **microblogging sucks**, but Twitter managed to make it
addictive for a few reasons, [which is *why* the most popular alternative,
Mastodon][masto], is basically a copy of the underlying formula, but tweaked to
work on the ActivityPub federated network (the so-called fediverse).

   [masto]: https://joinmastodon.org/

The formula is not that hard to understand if you understand how people
think and react to stimulation.  People are inherently social creatures,
and because of the formula used by Twitter, have tried their best to use
Twitter *despite* the inherent conceptual flaws behind microblogging.

If you've ever sat down at a slot machine, you will likely note that they are
constantly making noises as you interact with them.  These sounds are
designed to stimulate the reward center in your brain and thus cause it
to release endorphins.  In the same way, microblogging and other social
platform formulas have built rich notification systems to ensure that
users experience pleasure from being online.  Don't believe me?  Try
muting the notifications from Twitter or Mastodon and see if you remain
interested in it: odds are, after a while, you won't.

The other key part of the formula: sow discord amongst the users.  This
can be done organically (by users) or algorithmically.  People have an
inherent desire to be *right*, and this keeps the engagement loop going
as people fight over stupid things like whether Android or iPhones are
better.  The things being argued over do not even have to have any basis
in reality: people are more than happy to hold positions which falter to
any modicum of dialectical analysis, such as whether *furries are actually
shitting in litter boxes in schools* (obviously this is bullshit if you think
about it for more than 10 seconds).

Eventually these pointless arguments evolve into arguments which have
actual societal impact: *are trans people legitimate* and *do they deserve
rights*?  Obviously, they are, and they do, but in a world where
microblogging discourse is the primary form of media ingestion, the consumer
is manipulated with fight-or-flight challenges to make their own
280-character thought piece on the discourse of the day, which leads them
to consider the possibility that *perhaps* Chudlord18 *might* be on to something
when he points out that George Soros was seen at the last Bilderberg
meeting, entirely ignoring the part where Chudlord18's post was
disinformation.

Sadly, as we see in the world today, it turns out that fascism is the
most optimized ideology available given the limited cognitive bandwidth
constraints of a 280-character post.  This is because the answer is always
simple with fascism: generally a death threat towards the marginalized group
of the day will do just fine, which easily fits into 280 characters:
*"Storm the capitol building!"*?  *"Hang Mike Pence!"*?
Yep, even congressional members and vice presidents can be marginalized
under the right circumstances, *and* it's under 280 characters.

## Spamming and scamming

Fascism is hardly the only problem that these networks face.  Almost every
day I get spam like this on either Twitter or Mastodon:

![Spam messages for an affinity-fundraising scam from a Mastodon user](/images/mastodon-spam.png)

Spam like this is a huge problem with Mastodon, but not with Pleroma, another
ActivityPub server, which provides a robust message filtering facility.
However, due to the combination of mismanagement of the Pleroma project and
an absolutely absurd fediverse turf war, admins of Pleroma instances are
written off by some Mastodon admins as being evil, even if they are otherwise
harmless.

Between this and the architectural complexity of deploying a BEAM application
like Pleroma on Kubernetes, by comparison to how easy it is to deploy Mastodon
using Knative on Kubernetes, I am using Mastodon.  Since the project mismanagement
issues are largely resolved now, I might suck it up and convert the instance to
Pleroma in the near future just so I can deal with the spam in a more automated
way.

I'll probably continue to use Mastodon (or maybe Pleroma if I switch my instance
to it), but lately I've been using microblogging platforms less and less, as I
have realized that ultimately the format doesn't provide the sense of community
I am looking for.

And this is ultimately the problem with the fediverse: everything on the fediverse
is a clone of a proprietary platform, with basically the same social downsides.
It turns out when you take something useful, and turn it into a "social experience,"
you basically ruin its utility.

## Social tools which are actually respectful

To me, social tools exist to facilitate communication with my friends, and perhaps
expansion of my friend group to others which have the same interests.  It turns out
that we already had good social tools for this all along: blogs and IRC.  Because of
certain realities -- it is inherently easier to clone an open protocol and turn it
into a proprietary service -- for most people, these social tools turned into
centralized platforms like Dreamwidth and Discord.

Microblogging forces you to shout at people, while IRC (now for the most part Discord)
facilitates thoughtful conversation.  Social photo sharing encourages the editing of
photographs to make people appear more attractive for additional likes, while posting
photos of yourself to your blog removes that dopamine loop and lets you just focus on
living and occasionally documenting your life.

Yes, the point is that these tools are largely boring.  They aren't *meant* to dominate
your life, they are meant to facilitate communication with your friends.  They exist to
serve the needs of the commons.

Maybe somebody will eventually build the tools I am ultimately looking for.  In the
meantime, I've expanded my list of contact points to include services I previously
kept mostly private.

But either way, for the most part, I won't be investing my time in microblogging anymore,
be it on Twitter or Mastodon.
