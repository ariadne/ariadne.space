---
title: "the problematic GPL \"or later\" clause"
date: "2021-11-16"
---

The GNU General Public License started life as [the GNU Emacs Public License](https://www.free-soft.org/gpl_history/emacs_gpl.html) in 1987 (the linked version is from February 1988), and has been built on the principle of copyleft: [the use of the copyright system to enforce software freedom through licensing](https://www.gnu.org/licenses/copyleft.en.html). This prototype version of the GPL was used for other packages, such as GNU Bison (in 1988), and [Nethack (in 1989)](https://www.free-soft.org/gpl_history/nethack_gpl.html), and was most likely written by Richard Stallman himself.

This prototype version was also referred to as [the _GNU General Public License_ in a 1988 bulletin](https://www.gnu.org/bulletins/bull5.html#SEC5), so we can think of it in a way as GPLv0. This version of the GPL however, was mothballed, in Feburary 1989, [with the publication of the GPLv1](https://www.gnu.org/licenses/old-licenses/gpl-1.0.en.html). One of the new features introduced in the newly rewritten GPLv1 license, was the "or later" clause:

> 7\. The Free Software Foundation may publish revised and/or new versions of the General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.
> 
> Each version is given a distinguishing version number. If the Program specifies a version number of the license which applies to it and "any later version", you have the option of following the terms and conditions either of that version or of any later version published by the Free Software Foundation. If the Program does not specify a version number of the license, you may choose any version ever published by the Free Software Foundation.
> 
> Section 7 of the [GNU General Public License version 1](https://www.gnu.org/licenses/old-licenses/gpl-1.0.en.html)

The primary motive for the version upgrade clause, at the time, was quite simple: the concept of using copyright to enforce software freedom, was, at the time, a new and novel concept, and there was a concern that the license might have flaws or need clarifications. Accordingly, to streamline the process, they added the version upgrade clause to allow authors to consent to using new versions of the GPL as an alternative. Indeed, in the [January 1991 release of the GNU Bulletin](https://www.gnu.org/bulletins/bull10.html#SEC6), plans to release the GPLv2 were announced as an effort to clarify the license:

> We will also be releasing a version 2 of the ordinary GPL. There are no real changes in its policies, but we hope to clarify points that have led to misunderstanding and sometimes unnecessary worry.
> 
> GNU Bulletin volume 1, number 10, "[New library license](https://www.gnu.org/bulletins/bull10.html#SEC6)"

After that, not much happened in the GNU project regarding licensing for a long time, until the GPLv3 drafting process in 2006. From a governance point of view, the GPLv3 drafting process was a significant accomplishment in multi-stakeholder governance, as [outlined by Eben Moglen](https://softwarefreedom.org/resources/2013/A_History_of_the_GPLv3_Revision_Process.pdf).

However, for all of the success of the GPLv3 drafting process, it must be noted that the GPL is ultimately published by the Free Software Foundation, an organization that many have questioned the long-term viability of lately. When the "or later version" clause was first introduced to the GPL, it was unthinkable that the Free Software Foundation could ever be in such a state of affairs, but now it is.

And this is ultimately the problem: what happens if the FSF shuts down, and has to liquidate? What if an intellectual property troll acquires the GNU copyright assignments, or acquires the trademark rights to the FSF name, and publishes a new GPL version? There are many possibilities to be concerned about, but developers can do two things to mitigate the damage.

First, they can stop using the "or later" clause in new GPL-licensed code. This will, effectively, limit those projects from being upgraded to new versions of the GPL, which may be published by a compromised FSF. In so doing, projects should be able to avoid relicensing discussions, as GPLv3-only code is compatible with GPLv3-or-later: the common denominator in this case is GPLv3.

Second, they can stop assigning copyright to the FSF. In the event that the FSF becomes compromised, for example, by an intellectual property troll, this limits the scope of their possible war chest for malicious GPL enforcement litigation. As we have learned from the McHardy cases involving Netfilter, in a project with multiple copyright holders, effective GPL enforcement litigation is most effective when done as a class action. In this way, dilution of the FSF copyright assignment pool protects the commons over time from exposure to malicious litigation by a compromised FSF.
