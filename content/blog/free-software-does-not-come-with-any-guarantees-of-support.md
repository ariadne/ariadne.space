---
title: "free software does not come with any guarantees of support"
date: "2021-08-16"
---

This evening, I stumbled upon a Twitter post by an account which tracks features being added to GitHub:

<blockquote class="twitter-tweet"><p dir="ltr" lang="en">What if there was 📨 DM/inbox feature on GitHub?</p>— GitHub Projects (@GithubProjects) <a href="https://twitter.com/GithubProjects/status/1426795600071122947?ref_src=twsrc%5Etfw">August 15, 2021</a><script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></blockquote>

To be absolutely clear, this is a terrible idea.  Free software maintainers already have to deal with a subset of users who believe they are automatically entitled to support and, in some cases, SLAs from the maintainer.

Thankfully, the license I tend to use these days for my software makes it very clear that no support is provided:

> This software is provided 'as is' and without any warranty, express or implied. In no event shall the authors be liable for any damages arising from the use of this software.

However, it's not only my license which does this.  [So does the GPL](https://www.gnu.org/licenses/gpl-3.0.en.html), in all caps none the less:

> THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

Other free software licenses, such as the BSD license, and Apache 2.0 license also have a similar clause.  While maintainers might offer to provide support for free, or perhaps offer a support agreement for a fee, [free software is ultimately what you make of it](https://drewdevault.com/2021/06/14/Provided-as-is-without-warranty.html), for better or worse.

By integrating anti-features such as direct messages into software development forges like the proposed GitHub feature, the developers of these forges will inevitably be responsible for an onslaught of abusive messages directed at free software maintainers, which ultimately will serve to act as a Denial of Service on our time.  And, most likely, this abuse will be targeted [more frequently at maintainers who are women](https://deepdives.in/the-trouble-with-being-a-woman-in-foss-75181981bfdd), [or people of color](https://www.wired.com/2017/06/diversity-open-source-even-worse-tech-overall/).

I urge the developers of software development forges to respect the labor donated to the commons by free software maintainers, by developing features which respect their agency, rather than features that will ultimately lead abusive users to believe maintainers are available at their beck and call.

Otherwise, the developers of these forges may find projects going to a platform that _does_ respect them.
