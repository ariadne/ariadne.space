---
title: "AlpineConf 2021 recap"
date: "2021-05-18"
---

Last weekend was AlpineConf, the first one ever.  We held it as a virtual event, and over 700 participants came and went during the weekend.  Although there were many things we learned up to and during the conference that could be improved, I think that the first AlpineConf was a great success!  If you're interested in rewatching the event, both days have mostly full recordings on the [Alpine website](https://alpinelinux.org/conf).

## What worked

We held the conference on a [BigBlueButton](https://bigbluebutton.org) instance I set up and used the [Alpine Gitlab for organizing](https://gitlab.alpinelinux.org/alpine/alpineconf-cfp).  BigBlueButton scaled well, even when we had nearly 100 active participants, the server performed quite well.  Similarly, using issue tracking in Gitlab helped us to keep the CFP process simple.  I think in general, we will keep this setup for future events, as it worked quite well.

## What didn't work so well

A major problem with BigBlueButton was attaching conference talks from YouTube.  This caused problems with several privacy extensions which blocked the YouTube player from running.  Also, the YouTube video playback segments are missing from the recordings.  I'm going to investigate alternative options for this which should hopefully help with making the recorded talks play back correctly next time.

Maybe if a BigBlueButton developer sees this, they can work to improve the YouTube viewing feature as well so that it works on the recording playback.  That would be a really nice feature to have.

Other than that, we only had one scheduling SNAFU, and that was basically my fault -- I didn't confirm the timeslot I scheduled the cloud team talk in, and so naturally, the cloud team was largely asleep because they were in US/Pacific time.

Overall though, I think things went well and many people said they enjoyed the conference.  Next year, as we will have some experience to draw from, things will be even better, hopefully.

## The talks on day 1...

The first day was very exciting with a lot of talks and [blahaj representation](https://www.ikea.com/us/en/p/blahaj-soft-toy-shark-90373590/).  The talks mostly focused around user stories about Alpine.  We learned about where and how Alpine was being used... from phones, to data centers, to windmills, to the science community.  Here is the list of talks on the first day and my thoughts!

#### The Beauty of Simplicity, by Cameron Seid (@deltaryz)

This was the first talk of the conference and largely focused on how Cameron managed his Alpine server.  It was a good starting talk for the conference, I think, because it showed how people use Alpine at home in their personal infrastructure.  The talk was prerecorded and Cameron spent a lot of time on editing to make it look flashy.

#### pmbootstrap: The Swiss Army Knife of postmarketOS development, by Oliver Smith (@ollieparanoid)

postmarketOS is a distribution of Alpine for phones and other embedded devices.

In this talk, Oliver went into `pmbootstrap`, a tool which helps to automate many of the tasks of building postmarketOS images and packages.  About halfway through the talk, a user joined who I needed to make moderator, but I clicked the wrong button and made them presenter instead.  Thankfully, Oliver was a good sport about it and we were able to fix the video playback quickly.  I learned a lot about how `pmbootstrap` can be used for any sort of embedded project, and that opens up a lot of possibilities for collaborating with the pmOS team in other embedded applications involving Alpine.

#### Using Alpine Linux in DataCenterLight, by Nico Schottelius (@telmich)

In this talk, Nico walks us through how Alpine powers many devices in his data center project called DataCenterLight.  He is using Alpine in his routing infrastructure with 10 gigabit links!  The talk went over everything from routing all the way down to individual customer services, and briefly compared Alpine to Debian and Devuan from both a user and development point of view.

#### aports-qa-bot: automating aports, by Rasmus Thomsen (@Cogitri)

Rasmus talked about the `aports-qa-bot` he wrote which helps maintainers and the mentoring team review merge requests from contributors.  He went into some detail about the modular design of the bot and how it can be easily extended for other teams and also Alpine derivatives.  The postmarketOS team asked about deploying it for their downstream `pmaports` repo, so you'll probably be seeing the bot there soon.

#### apk-polkit-rs: Using APK without the CLI, by Rasmus Thomsen (@Cogitri)

Rasmus had the next slot as well, where he talked about his `apk-polkit-rs` project which provides a DBus service that can be called for installing and upgrading packages using apk.  He also talked about the rust crate he is working on to wrap the apk-tools 3 API.  Overall, the future looks very interesting for working with apk-tools from rust!

#### Alpine building infrastructure update, by Natanael Copa (@ncopa)

Next, Natanael gave a bubble talk about the Alpine building infrastructure.  For me this was largely a trip down memory lane, as I witnessed the build infrastructure evolve first hand.  He talked about how the first generation build infrastructure was a series of IRC bots which reacted to IRC messages in order to trigger a new build, and how the IRC infrastructure evolved from IRC to ZeroMQ to MQTT.

He then showed how the builders work, using a live builder as an example, walking through the design and implementation of the build scripts.  Finally, he proposed some ideas for building a more robust system that allowed for parallelizing the build process where possible.

#### postmarketOS demo, by Martijn Braam (@MartijnBraam)

Martijn showed us postmarketOS in action on several different phones.  Did I mention he has a lot of phones?  I asked in the Q&A afterwards and he said he had like 6 pinephones and somewhere around 60 other phones.

I have to admire the dedication to reverse engineering phones that would lead to somebody acquiring 60+ phones to tinker with.

#### Sxmo: Simple X Mobile - A minimalist environment for Linux smartphones, by Maarten van Gompel (@proycon)

Maarten van Gompel, Anjandev Momi and Miles Alan gave a talk about and demonstration of Sxmo, their lightweight phone environment based on dwm, dmenu and a bunch of other tools as plumbing.

The UI reminds me a lot of palmOS.  I suspect if palmOS were still alive and kicking today, it would look like Sxmo.  **Phone calls and text messages are routed through shell scripts**, a feature I didn't know I needed until I saw it in action.  Sxmo probably is _the_ killer app for running an actual Linux distribution on your phone.

This UI is absolutely _begging_ for jog-wheels to come back, and I for one hope they do.

#### Alpine and the larger musl ecosystem (a roundtable discussion)

This got off to a rocky start because I don't know how to organize stuff like this.  I should have found somebody else to run the discussion, but it was really fruitful.  We came to the conclusion that we needed to work more closely together in the musl distribution ecosystem to proactively deal with issues like misinformed upstreams and so on, so that we do not have another Rust-like situation again.  That lead to the formation of `#musl-distros` on freenode to coordinate on these issues.

#### Taking Alpine to the Edge and Beyond With Linux Foundation's Project EVE, by Roman Shaposhnik (@rvs)

Roman talked about Project EVE, an edge computing solution being developed under the auspices of the LF Edge working group at Linux Foundation.  EVE (Edge Virtualization Engine) is a distribution of Alpine built with Docker's LinuxKit, which has multiple Alpine-based containers working together in order to provide an edge computing solution.

He talked about how the cloud has eroded software freedom (after all, you can't depend on free-as-in-freedom computing when it's on hardware you don't own) by encouraging users to trade it for convenience, and how edge computing brings that same convenience in-house, thus solving the software freedom issue.

Afterward, he demonstrated how EVE is deployed on windmills to analyze audio recordings from the windmill to determine their health.  All of that, including the customer application, is running on Alpine.

He concluded the talk with a brief update on the `riscv64` port.  It looks like we are well on the way to having the port in Alpine 3.15.

#### BinaryBuilder.jl: The Subtle Art of Binaries that "Just Work", by Elliot Saba and Mosè Giordano

Elliot and Mosè talked about BinaryBuilder, which they use to cross-compile software for all platforms supported by the Julia programming language.  They do this by building the software in an Alpine-based environment under Linux namespaces or Docker (on mac).

Amongst other things, they have a series of wrapper scripts around programs like `uname` which allow them to emulate the userspace commands of the target operating system, which helps convince badly written autoconf scripts to cooperate.

All in all, it was a fascinating talk!

## The talks on day 2...

The talks on day 2 were primarily about the technical plumbing of Alpine.

#### Future of Alpine Linux community chats (a roundtable discussion)

We talked about the [current situation on freenode](https://news.ycombinator.com/item?id=27153338).  The conclusion we came to regarding that was to support the freenode staff in their efforts to find a solution until the end of the month, at which point we would evaluate the situation again.

This lead to a discussion about enhancing the IRC experience for new contributors, and the possibility of just setting up an internal IRC server for the project to use, as well as working with Element to set up a hosted Matrix server alternative.

We also talked for the first time about the Alpine communities which are growing on non-free services such as Discord.  Laurent observed that there is value in meeting users where they already are for outreach purposes, and also pointed out that the nature of proprietary IRC networks imposes a software freedom issue that doesn't exist with self-hosting our own.  Most people agreed with these points, so we concluded that we would figure out plans to start integrating these unofficial communities into Alpine properly.

#### Security tracker demo and security team Q&A

This was kind of a bubble talk.  I gave a demo of the new security.alpinelinux.org tracker, as well as an overview of how the current CVE system works with the NVD and CIRCL feeds and so on.  We then talked a bit about how the CVE system could be improved by the Linked Data proposal I am working on, which will be published shortly.

Afterwards, we talked about initiatives like bringing `clang`'s Control Flow Integrity into Alpine and a bunch of other topics about security in Alpine.  It was a fun talk and we covered a lot of topics.  It went for an hour and a half, as a talk was cancelled in the 15:00 slot.

#### Alpine s390x port discussion, by me

After the security talk, I talked a bit about running Alpine on mainframes, how they work, and why people still want to use them in 2021.  In the Q&A we talked about big vs little endian and why people aren't mining Monero on mainframes.

#### Simplified networking configuration with ifupdown-ng, by me

This was an expanded talk about ifupdown-ng loosely based on the one Max gave at VirtualNOG last year.  I adapted his talk, replacing Debian-specific content with Alpine content and talked a bit about NSL (RIP).  The talk seemed to go well, in the Q&A we talked primarily about SR-IOV, which is not yet supported by ifupdown-ng.

#### Declarative networking configuration with ifstate, by Thomas Liske (@liske)

After the ifupdown-ng talk, Thomas talked about and demonstrated his `ifstate` project, which is available as an alternative to `ifupdown` in Alpine.  Unlike ifupdown-ng which takes a hybrid approach, and ifupdown which takes an imperative approach, ifstate is a fully declarative implementation.  The YAML syntax is quite interesting.  I think ifstate will be quite popular for Alpine users requiring fully declarative configuration.

#### AlpineConf 2.0 planning discussion

After the networking track, we talked about AlpineConf next year.  The conclusion was that AlpineConf has most value being a virtual event, and that if we want to have a physical event there's events like FOSDEM out there which we can use for that.

#### Alpine cloud team talk and Q&A

This wound up being a bit of a bubble talk because I failed to actually confirm whether anyone from the cloud team could give a talk at this time.  Nonetheless the talk was a huge success.  We talked about Alpine in the cloud and how to build on it.

#### systemd: the good parts, by Christine Dodrill (@Xe)

Christine gave a talk about systemd's feature set that she would like to see implemented in Alpine somehow.  In the chat, Laurent provided some commentary...

It was a fun talk that was at least somewhat amusing.

#### Governance event

Finally to close out the conference, Natanael talked about Alpine governance.  In this event, he announced the dissolution of the Alpine Core Team and implementation of the Alpine Council instead.  The Alpine Council will be initially managed by Natanael Copa, Carlo Landmeter and Kevin Daudt in the interim.  This group will handle the administrative responsibilities of the project, while a technical steering committee will handle the technical planning for the project.  This arrangement is likely familiar to anyone who has used Fedora, I think it makes sense to copy what works!

Afterwards, we talked a little bit informally about everyone's thoughts on the conference.

## In closing...

Thanks to [Natanael Copa for proposing the idea of AlpineConf last year](https://lists.alpinelinux.org/~alpine/devel/%3C20200521160527.718c2d2c%40ncopa-desktop.copa.dup.pw%3E), Kevin Daudt for helping push the buttons and keeping things going (especially when my internet connection failed due to bad weather), all of the wonderful presenters who gave talks (many of which gave talks for their first time ever!) and everyone who dropped in to participate in the conference!

We will be having a technically-oriented Alpine miniconf in November, and then AlpineConf 2022 next May!  Hopefully you will be at both.  Announcements will be forthcoming about both soon.
