---
title: "Help migrate a community from Discord to something else"
date: "2023-03-08"
---

During the height of the pandemic, I set up a community using Discord.
Since then, it has evolved into being one of the most active (yet tight-knit) technical
communities on Discord: members ranging from all around the world and from all sorts of
technical and social backgrounds participate in conversations every day on a variety of
topics.

## Why leave Discord?

The current situation sounds pretty good, right?
Well, as Richard Stallman warned, [proprietary services masquerading as software][gnu-saass]
do not necessarily act on behalf of the user.

   [gnu-saass]: https://www.gnu.org/philosophy/who-does-that-server-really-serve.en.html

In this specific case, despite paying money to Discord for its services, there have been
many instances where it has been transparently obvious to myself and the rest of our team
that Discord is not really acting in the interest of our community.
Some examples:

* Discord has banned the accounts of several community members over the past 18 months.
  When pressed on the issue, they usually have no viable explanation for why they took
  that specific action.

* Discord has rolled out automated moderation features which were configured with very
  aggressive defaults, and enabled by default.
  These features have also had bugs, and when pressing support on those issues, our
  mileage has varied.
  We have been able to mostly disable the "auto-mod" features that were obnoxiously
  intrusive, however.

* Discord's leadership team has speculated on introducing functionality that is not
  aligned with the interests of our community, such as NFT support.
  They later rolled this speculation back after too many users complained.

On March 27th, Discord plans to roll out a [new Privacy Policy][new-pp] which,
among other things, grants them the right to record video calls without consent.
It is very likely that they plan to do this in order to enforce Content ID type
restrictions on the content being shared in Discord-using communities.

Although the community I started does not frequently share content which would run
afoul of these issues, Content ID systems can be easily fooled into reacting to
content which does not violate any copyright, such as ambient noises.
On top of this, the community in question engages in a lot of activism on various
topics intersectional to the world we exist in.
Discord being able to step into and monitor video calls in our community is
therefore entirely unacceptable from a security point of view.

   [new-pp]: https://discord.com/privacy

Should we have chosen Discord for this community given its' security needs?
Most likely not, but at the time that this community was established, the
current reality was not envisioned.
Had we intended to build a community explicitly for the activities that it
has chosen to engage in, we would likely have avoided Discord.
But at the same time, the Discord UX is likely attributable for some of the
success of the community: it is simple and largely optimized for the
multi-device reality we now live in.

## Matrix?

During the height of the pandemic, FOSDEM was held virtually on Matrix-based
infrastructure.
The combination of multimedia experiences and text-based chat looked to be
marginally competitive to the formula that Discord provides communities.
For this reason, Matrix is at the top of a short list of alternatives we are
considering.

But we have questions and concerns.
This is where a helpful advocate from the Matrix community who is familiar
with the Trust & Safety aspects of the protocol would be welcome.
I would even be willing to pay a reasonable consultancy fee for real answers
to these concerns.

The main concern we have is one of safety.
Much like with the fediverse, there are homeservers run by persons known to
be a security threat to our community.
We need a robust solution for keeping those homeservers defederated from the
rooms hosted on our own homeserver.

We are told that Synapse has an integration with Mjolnir to allow for this,
but we would prefer to use Dendrite.
Does Dendrite offer a similar integration with Mjolnir?
Also, with a bot making real-time policy decisions on what room invitations
and joins are allowed, is it possible to have physical redundancy for
Mjolnir?

Building on that question, availability in general is another major area
of concern.
Is it possible to have multiple instances of Dendrite running at once in
a geo-distributed fashion?
It is okay to assume that we would be using Spanner or similar to manage
the database replication to support that.

The next major concern is CSAM.
When we launched our Mastodon instance, we had an incident where a user
uploaded CSAM to our instance.
We have heard that it is possible to convince homeservers to blindly
cache CSAM from other homeservers.
What mitigations exist for this issue?

Finally, the last remaining question is how to integrate this into our
infrastructure.
For reference, we use Kubernetes to manage our services, with Traefik
acting both as Ingress and as a Service Mesh.
Previously we used Knative to manage some services such as Mastodon Web.
Is there any advantage to using Knative to manage a Matrix homeserver?
(We assume there is not.)

## Something else?

We are also open to using something other than Matrix.
But it needs to be something that we can manage ourselves at the
infrastructure level.
We already have been burned by Discord, we're not interested in being
burned by another service.
It also needs to provide a similar end-to-end user experience as
Discord.
From what I can find, Matrix is the only project out there that is
able to meet those requirements.

But I would be happy to hear about alternatives which have done
similarly well at getting over the hump, where network effect is no
longer a serious concern.

Reach me at <ariadne@dereferenced.org> if you have answers to any of
the above questions.  Thanks!
