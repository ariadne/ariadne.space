---
title: "a tail of two bunnies"
date: "2021-08-21"
---

As many people know, I collect stuffed animals.  Accordingly, I get a lot of questions about what to look for in a quality stuffed animal which will last a long time.  While there are a lot of factors to consider when evaluating a design, I hope the two examples I present here in contrast to each other will help most people get the basic idea.

## the basic things to look for

A stuffed animal is basically a set of fabric patches sewn together around some stuffing material.  Therefore, the primary mode of failure for a stuffed animal is when one or more seams suffers a tear or rip in its stitching.  A trained eye can look at a design and determine both the likelihood of failure and the most vulnerable seams, even in a high quality stuffed animal.

There are two basic ways to sew together a stuffed animal: the fabric patches can be sewn together to form inward-facing seams, or they can be sewn together to form outward-facing seams.  Generally, the stuffed animals that have inward facing seams have more robust construction.  This means that if you can easily see the seam lines that the quality is likely to be low.  Similarly, if eyes and other accessories are sewn in along a main seam line, they become points of vulnerability in the design.

Materials also matter: if the purpose of the stuffed animal is to be placed on a bed, or in a crib, it should be made out of fire-retardant materials.  Higher quality stuffed animals will use polyester fill with a wool-polyester blend for the outside, while lower quality stuffed animals may use materials like cotton.  In the [event of a fire](https://www.sikkerhverdag.no/en/safe-products/clothes-and-equipment/these-clothes-are-the-most-flammable/), polyester can potentially melt onto skin, but materials like cotton will burn much more vigorously than polyester (which is fire retardant).

Finally, it is important to verify that the stuffed animal has been certified to a well-known safety standard.  Look for compliance with the European Union's EN71 safety standard or the ASTM F963 standard.  Do not buy any stuffed animal made by a company which is not compliant with these standards.  Stuffed animals bought off maker-oriented websites like Etsy will most likely not be certified, in these cases, you may wish to verify with the maker that they are familiar with the EN71 and ASTM F963 standards and have designed around those standards.

## a good example: the jellycat bashful bunny

![A jellycat bashful bunny, cream colored, size: really big.  it is approximately 4 feet tall.](/images/BARB1BC-300x300.jpg)

One of my favorite bunny designs is the [Jellycat Bashful Bunny](https://www.jellycat.com/us/bashful-cream-bunny-bas3bc/).  I have several of them, ranging from small to the largest size available.

This is what I would consider to be a high quality design.  While the seam line along his tummy is visible, it is a very small seam line, which is indicative that the stitching is inward-facing.  There are no other visible seam lines.  Cared for properly, this stuffed animal will last a very long time.

## a bad example: build a bear's pawlette

![Jumbo Pawlette, from build a bear.  This variant is 3 feet tall.](/images/25756Alt1x-300x300.jpg)

A few people have asked me about [Build a Bear's Pawlette design](https://www.buildabear.com/online-exclusive-jumbo-pawlette/025756.html) recently, as it looks very similar to the Jellycat Bashful Bunny.  I don't think it is a very good design.

To start with, you can see that there are 21 separate panels stitched together: 4 for the ears, 3 for the head, 4 for the arms, 2 for the tummy, 2 for the back, 4 for the legs, and 2 for the feet.  The seam lines are very visible, which indicates that there is a high likelihood that the stitching is outward rather than inward.  That makes sense, because it's a lot easier to stitch up a stuffed animal in store that way.  Additionally, you can see that the eyes are anchored to the seam lines that make up the face, which means detachment of the eyes is a likely possibility as a failure mode.

Build a Bear has some good designs that are robustly constructed, but Pawlette is not one of them.  I would avoid that one.

Hopefully this is helpful to somebody, at the very least, I can link people to this post now when they ask about this stuff.
