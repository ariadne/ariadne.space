---
title: "the whole freenode kerfluffle"
date: "2021-05-20"
---

> But the thing is IRC has always been a glorious thing. The infra has always been sponsored by companies or people. But the great thing about IRC is you can always vote and let the networks and world know which you choose - by using /server.
> 
> — Andrew Lee (rasengan), chairman of freenode limited

Yesterday, operational control over freenode was taken over by Andrew Lee, the person [who has been owner of freenode limited since 2017](https://find-and-update.company-information.service.gov.uk/company/10308021/officers).  Myself and others have had questions about this arrangement since we noticed the change in ownership interest in freenode limited back in 2017.

Historically, freenode staff had stated that everything was under control and that Andrew's involvement in freenode limited had no operational impact on the network.  It turns out that Christel was lying to them: Andrew had operational control and legal authority over the freenode domains.  This lead to [several current volunteers drafting their resignation letters](https://fuchsnet.ch/freenode-resign-letter.txt).

When I asked Andrew about the [current state of the freenode domain](http://distfiles.dereferenced.org/stuff/rasengan-log.txt), one of his associates who I hadn't spoken to in months (since terminating the Ophion project I was doodling on during lockdown) came out of nowhere and started offering me [bribes of staff privileges and money for Alpine](https://distfiles.dereferenced.org/stuff/nirvana-log.txt).  These developments were concerning to the Alpine council and interim technical committee, so we scheduled an event at AlpineConf to talk about the situation.

Our initial conclusion was that we should wait until the end of the month and see how the situation shakes out, and possibly plan to stand up our own IRC infrastructure or use another network.  Then this happened yesterday:

\[02:54:38\] <-- ChanServ (ChanServ@services.) has quit (Killed (grumble (My fellow staff so-called 'friends' are about to hand over account data to a non-staff member. If you care about your data, drop your NickServ account NOW before that happens.)))

Given that situation, members of the Alpine council and technical committee gathered together to discuss the situation.  We decided to move to OFTC immediately, as we wanted to give users the widest window of opportunity to delete their data.  This move [has now been concluded](https://alpinelinux.org/posts/Switching-to-OFTC.html), and I appreciate the help of the OFTC IRC network staff as well as the Alpine infrastructure team to migrate all of our IRC-facing services across.  The fact that we were able to move so quickly without much disruption is a testament to the fact that IRC and other open protocols like it are vital for the free software community.

## So, why does he want to control freenode anyway?

I have had the pleasure of using freenode since 2003, and have been a staff member on several occasions.  My work on IRC, such as starting the IRCv3 project and writing charybdis and atheme, was largely motivated by a desire to improve freenode.  It is unfortunate that one person's desire for control over an IRC network has lead to so much destruction.

But why is he actually driven to control these IRC networks?  Many believe it is about data mining, or selling the services database, or some other boring but sensible explanation.

But that's not why.  What I believe to be the real answer is actually much sadder.

I spent several months talking to Andrew and his associate, Shane, last year during lockdown while I was [writing an IRCX server](https://github.com/ophion-project/ophion) (I didn't have much to do last summer during lockdown and I had always wanted to write an IRCX server).  Shane linked a server to my testnet because he was enthusiastic about IRCX, he had previously been a user on `irc.msn.com`.  Both he and Andrew acted as IRCops on the server they linked.  In that time, I learned a lot about both of them, what their thought processes are, how they operate.

In December 2018, Andrew acquired the irc.com domain.  On that domain, he wrote a post titled [Let's take IRC further](http://web.archive.org/web/20181207230330/https://www.irc.com/lets-take-irc-further).  Based on this post, we can gather a few details about Andrew's childhood: he grew up as a marginalized person, and as a result of that marginalization, he was bullied.  IRC was his outlet, a space for him that was actually safe for him to express himself.  Because of that, he was able to learn about technology and free software.

Because of this, I believe Andrew's intention is to preserve IRC as it was formative in his transition from childhood to adulthood.  He finds IRC to be comforting, in the same way that I find the [bunny plushie](https://www.jellycat.com/us/bashful-cream-bunny-bas3bc/) I sleep with to be comforting.  This is understandable to me, as many people strongly desire to preserve the environment they proverbially grew up in.

However, in implementing his desire to preserve the IRC network he grew up on, he has effectively destroyed it: projects are leaving or planning to leave en masse, which is sad.

Whether you want to participate in [Andrew's imaginary kingdom or not](https://en.wikipedia.org/wiki/Role-playing) is up to you, but I believe the current situation to be untenable for the free software community.  We cannot depend on an IRC network where any criticism of Andrew may be perceived by him as a traumatic experience.

I strongly encourage everyone to move their projects to either [OFTC](https://oftc.net/) or [Libera Chat](https://libera.chat/).  I will be disconnecting from freenode on May 22nd, and I have no plans to ever return.

And to the volunteers who kept the network going, with whom I had the privilege on several occasions over the years of working with: I wish you luck with Libera Chat.
