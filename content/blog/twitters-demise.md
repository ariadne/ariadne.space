---
title: "Twitter's demise is ActivityPub's future"
date: '2022-11-12'
---

Earlier today, I deleted all of my tweets and left Twitter forever.  While I
plan on leaving a nightlight thread for a while, I will eventually close my
account, assuming Elon doesn't do it for me.

The past week has been an emotional rollercoaster for me as I have watched
everything play out.

I was one of the original fediverse users when Indymedia UK stood up the
`indy.im` StatusNet instance at the end of 2010.
After some time, Evan Prodromou got bored with the StatusNet code base
and started Pump instead, with the network losing the largest instance
at that time, `identi.ca`.
With the network fragmented as a result of that switch, I got bored of
it and started using Twitter instead.

Eventually StatusNet was forked by Matt Lee and a few other FSF staffers and
became GNU Social.
I was not really around during this time, but it was around that time that
GamerGate happened, which created a network where half of the users were
Indymedia contributors and the other half were the initial seeds of the
alt-right.

While I was not heavily involved from a development perspective in the early
days of what we now call the fediverse, this began to change in late 2016 when
Eugen Rochko started Mastodon.
I was an early adopter of Mastodon, deploying Mastodon 0.6 on Heroku, using the
`mastodon.dereferenced.org` domain for my account.
But running Mastodon on Heroku (and later Scalingo) was expensive.  I did not
want to manage a Rails application by hand, and I hadn't started using Docker or
Kubernetes yet.

In early 2018, a developer psuedonymously known as lain began adding ActivityPub
federation support to Pleroma, and he convinced me to try it out as an alternative
to running Mastodon.
I found Pleroma and developing with Elixir to be exciting and fresh, compared to
other technology I was working with at the time.  I felt empowered to start doing
serious hacking on ActivityPub as a result of writing patches to Pleroma and
sending them to lain.

After a while, I became a Pleroma developer with commit rights.
I felt like we could use the same strategy I used to promote Alpine to promote Pleroma:
build a coalition of willing influencers to demonstrate the value proposition of
self-hosted social networks for user freedom, and so I started working on building
a group around it.
Because I was showing it to friends I already had, Pleroma grew into being a
project where many of the contributors were from queer and marginalized backgrounds
similar to mine.
Everything was going fine.  As a team, we built a lot of features that are still
innovative in this space, such as MRF and building the LitePub profile of ActivityPub,
which shifted the protocol from being a Content *Distribution* protocol to being a
Content *Advertisement* protocol.

Towards the end of 2019, it started going to shit.  By that time, I was running a
public instance, and the database kept having index corruption issues on a daily
basis.
Around the same time, the Soapbox project was launched, and they decided to use
Pleroma as their backend.  This led to a lot of friction inside the project, because
the Soapbox author had a tendency to share [his ideological positions][ag-trans]
inside the project space as part of his anti-trans activism.
I wound up leaving Pleroma toward the middle of 2020 because of the scalability
issues in the database with Pleroma 2.0 and the lack of any effort to maintain a
welcoming space for everyone.

   [ag-trans]: https://blog.alexgleason.me/trans/

I decided to take a break from the fediverse because of that decision, because I
felt a break was warranted.  I decided to try Twitter in earnest during that time,
but to be honest, I've never found using Twitter to be enjoyable in the same way
as I found the fediverse to be enjoyable.

As I said a few weeks ago, I think that [commercial microblogging][cmb] has been
an absolute disaster for our society.  Relationships on Twitter are parasocial
and transactional, which leads to poisonous behavior, while relationships in the
fediverse are largely grounded and mutual.

   [cmb]: https://ariadne.space/2022/10/27/the-internet-is-broken-due-to-structural-injustice/

In April of this year, Elon Musk announced his intention to buy Twitter.  Based
on the experience of watching a [rich fanatic purchase and then ruin something he
deeply cared about][leenode] and my experience of being a Tesla owner, I thought it
would be relevant to set up an [escape hatch][th-masto].  Others were of the same
mind, and we shared notes.

   [leenode]: https://ariadne.space/2021/05/20/the-whole-freenode-kerfluffle/
   [th-masto]: https://social.treehouse.systems/

With the events of the past few weeks, I strongly believe that Twitter's demise is
going to bring all of the proprietary social silos crashing down.  People are starting
to realize that trading freedom for the alleged convenience of using a proprietary
network isn't worth it.  Although not perfect, ActivityPub is eating the world: there's
now a million new users a week and this number is growing.

### ... which brings me to the not so fun part, the things that aren't going so well.

Although the fediverse is a decentralized and disparate network with many different
groups with their own cultural norms, some of them have tried to enforce their cultural
norms on the new users.  This is normal and to be expected to some extent, as people
don't like big changes.

I don't want to get into the nuances of some of these conversations.  What I do want
to say is that the fediverse is a diverse network of different people who bring their
own styles and approaches to posting and content curation.  It is entirely fine to
bring your whole self to the conversation in uncensored form if that is what you feel
is right to do.  Do what *you* feel is right, and don't worry about people muting or
blocking your account, because you're not here for *them*, you're here for *yourself*
and you will meet likeminded people regardless of who blocks you.

The other problem is, of course, a question of scaling anti-abuse tools.  Many have
posted screenshots of abuse they have received, and it comes from a segment of the
larger network where the culture is most diplomatically described as "player vs
player."  It is fine for those instances to exist, but we need to build better tools
so that newcomers can be aware of segments of the network that they may want to
exclude themselves from: what we have today where admins informally share threat data
with each other is hard to scale upwards.

In general these are good problems to have, because they are easy to overcome.  Overall
the future is looking bright.

### Which instances are you recommending right now?

At the moment, I am trying to recommend instances which have a moderation policy
aligned with providing a safe space for marginalized identities like mine which
are also targeted at technical people.

Some recommendations:

- [hachyderm.io](https://hachyderm.io), running Mastodon 3.5.3 and administrated
  by Kris Nova and other volunteers.

- [social.restless.systems](https://social.restless.systems), running Mastodon 4.0 and
  administrated by NCommander, a tech YouTuber.

- [social.treehouse.systems](https://social.treehouse.systems), run by me and other
  volunteers.  It also runs Mastodon 4.0.

The reasons why I recommend these instances are because the administrative capabilities
far exceed those required by the Mastodon Server Covenant: the above instances are run
by teams with marginalized backgrounds and extensive SRE experience.

I am planning to put together a larger tool for finding instances which have been stood
up as part of this new wave of SRE-backed quasi-professional instances.

### What next?

Next time, I will write a bit about how my own instance is put together and how it has
evolved over the past few months.  Stay tuned for that one.
