---
title: "the end of freenode"
date: "2021-06-14"
coverImage: "BitchX_logo_-_ACiD.png"
---

My first experience with IRC was in 1999.  I was in middle school, and a friend of mine ordered a [Slackware CD from Walnut Creek CDROM](https://web.archive.org/web/19980209123157/http://ftp.cdrom.com/titles/os/os.htm#linux_distributions).  This was Slackware 3.4, and contained the GNOME 1.x desktop environment on the disc, which came with the [BitchX IRC client](http://bitchx.sourceforge.net/).

At first, I didn't really know what BitchX was, I just thought it was a cool program that displayed random ascii art, and then tried to connect to various servers.  After a while, I found out that an IRC client allowed you to connect to an IRC network, and get help with Slackware.

At that time, freenode didn't exist.  The Slackware IRC channel was on DALnet, and I started using DALnet to learn more about Slackware.  Like most IRC newbies, it didn't go so well: I got banned from `#slackware` in like 5 minutes or something.  I pleaded for forgiveness, in the way redolent of a middle schooler.  And eventually, I got unbanned and stuck around for a while.  That was my first experience with IRC.

After a few months, I got bored of running Linux and reinstalled Windows 98 on my computer, because I wanted to play games that only worked on Windows, and so, largely, my interest in IRC waned.

A few years passed... I was in eighth grade.  I found out that one of the girls in my class was [a witch](https://en.wikipedia.org/wiki/Wicca).  I didn't really understand what that meant, and so I pressed her for more details.  She said that she was a Wiccan, and that I should read more about it on the Internet if I wanted to know more.  I still didn't quite understand what she meant, but I looked it up on [AltaVista](https://en.wikipedia.org/wiki/AltaVista), which linked me to an entire category of sites on [dmoz.org](https://en.wikipedia.org/wiki/DMOZ).  So, I read through these websites and on one of them I saw:

> Come join our chatroom on DALnet: `irc.dal.net #wicca`

DALnet!  I knew what that was, so I looked for an IRC client that worked on Windows, and eventually installed [mIRC](https://mirc.co.uk/).  Then I joined DALnet again, this time to join `#wicca`.  I found out about a lot of other amazing ideas from the people on that channel, and wound up joining others like `#otherkin` around that time.  Many of my closest friends to this day are from those days.

At this time, DALnet was the largest IRC network, with almost 150,000 daily users.  Eventually, my friends introduced me to mIRC script packs, like [NoNameScript](https://archives.darenet.org/?dir=irc/scripts/mirc/nonamescript), and I used that for a few years on and off, sometimes using BitchX on Slackware instead, as I figured out how to make my system dual boot at some point.

## The DALnet DDoS attacks

For a few years, all was well, until the end of July 2002, when DALnet started being the target of [Distributed Denial of Service](https://en.wikipedia.org/wiki/Denial-of-service_attack#Distributed_DoS) attacks.  We would of course, later find out that these attacks were at the request of [Jason Michael Downey (Nessun), who had just launched a competing IRC network called Rizon](https://en.wikipedia.org/wiki/Operation:_Bot_Roast).

However, this resulted in `#slackware` and many other technical channels moving from DALnet to `irc.openprojects.net`, a network that was the predecessor to freenode.  Using `screen`, I was able to run two copies of the BitchX client, one for freenode, and one for DALnet, but I had difficulties connecting to the DALnet network due to the DDoS attacks.

## Early freenode

At the end of 2002, `irc.openprojects.net` became freenode.  At that time, freenode was a much different place, with community projects like `#freenoderadio`, a group of people who streamed various 'radio' shows on an Icecast server.  Freenode had less than 5,000 users, and it was a community where most people knew each other, or at least knew somebody who knew somebody else.

At this time, freenode ran `dancer-ircd`, with `dancer-services`, which were written by the Debian developer Andrew Suffield and based on ircd-hybrid 6 and HybServ accordingly.

Dancer had a lot of bugs, the software would frequently do weird things and the services were quite spartan compared to what was available on DALnet.  I knew based on what was available over on DALnet, that we could make something better for freenode, and so I started to learn about IRCD.

## Hatching a plan to make services better

By this time, I was in my last year of high school, and was writing IRC bots in Perl.  I hadn't really tried to write anything in C yet, but I was learning a little bit about C by playing around with a test copy of UnrealIRCd on my local machine.  But I started to talk to `lilo` about improving the services.  I knew it could be done, but I didn't know how to do it yet, which lead me to start searching for services projects that were simple and understandable.

In my searching for services software, I found `rakaur`'s [Shrike project](https://github.com/rakaur/shrike), which was a very simple clone of Undernet's X service which could be used with ircd-hybrid.  I talked with `rakaur`, and I learned more about C, and even added some features.  Unfortunately, we had a falling out at that time because a user on the network we ran together found out that he could make `rakaur`'s IRC bot run `rm -rf --no-preserve-root /`, and did so.

After working on Shrike a bit, I finally knew what to do: extend Shrike into a full set of DALnet-like services.  I showed what I was working on to `lilo` and he was impressed: I became a freenode staff member, and continued to work on the services, and all went well for a while.  He also recruited my friend `jilles` to help with the coding, and we started fixing bugs in `dancer-ircd` and `dancer-services` as an interim solution.  And we started writing `atheme` as a longer-term replacement to `dancer-services`, originally under the auspices of freenode.

## Spinhome

In early 2006, `lilo` launched his Spinhome project.  Spinhome was a fundraising effort so that `lilo` could get a mobile home to replace the double-wide trailer he had been living in.  Some people saw him trying to fundraise while being the owner of freenode as a conflict of interest, which lead to a falling out with a lot of staffers, projects, etc.  OFTC went from being a small network to a much larger network during this time.

One side effect of this was that the `atheme` project got spun out into its own organization: atheme.org, which continues to exist in some form to this day.

The atheme.org project was founded on the concept of promoting _digital autonomy_, which is basically the network equivalent of software freedom, and has advocated in various ways to preserve IRC in the context of digital autonomy for years.  In retrospect, some of the ways we advocated for digital autonomy were somewhat obnoxious, but as they say, hindsight is always 20/20.

## The hit and run

In September 2006, `lilo` was hit by a motorist while riding his bicycle.  This lead to a managerial crisis inside freenode, where there were two rifts: one group which wanted to lead the network was lead by Christel Dahlskjaer, while the other group was lead by Andrew Kirch (`trelane`).  Christel wanted to update the network to use all of the new software we developed over the past few years, and so atheme.org gave her our support, which convinced enough of the sponsors and so on to also support her.

A few months later, `lilo`'s brother tried to claim title to the network to turn into some sort of business.  This lead to Christel and Richard Hartmann (`RichiH`) meeting with him in order to get him to back away from that attempt.

After that, things largely ran smoothly for several years: freenode switched to `atheme`, and then they switched to `ircd-seven`, a customized version of `charybdis` which we had written to be a replacement for `hyperion` (our fork of `dancer-ircd`), after which things ran well until...

## Freenode Limited

In 2016, Christel incorporated freenode limited, under the guise that it would be used to organize [the freenode #live conferences](https://freenode.live).  In early 2017, she sold 66% of her stake in freenode limited to Andrew Lee, who I wrote [about in last month's chapter](https://ariadne.space/2021/05/20/the-whole-freenode-kerfluffle/).

All of that lead to Andrew's takeover of the network last month, and last night they decided to remove the `#fsf` and `#gnu` channels from the network, and k-lined my friend Amin Bandali when he criticized them about it, which means freenode is definitely no longer a network about FOSS.

Projects should use alternative networks, like OFTC or Libera, or better yet, operate their own IRC infrastructure.  Self-hosting is really what makes IRC great: you can run your own server for your community and not be beholden to anyone else.  As far as IRC goes, that's the future I feel motivated to build.

This concludes my coverage of the freenode meltdown.  I hope people enjoyed it and also understand why freenode was important to me: without `lilo`'s decision to take a chance on a dumbfuck kid like myself, I wouldn't have ever really gotten as deeply involved in FOSS as I have, so to see what has happened has left me heartbroken.
