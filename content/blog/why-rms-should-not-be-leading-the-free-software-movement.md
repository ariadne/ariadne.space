---
title: "Why RMS should not be leading the free software movement"
date: "2021-03-23"
---

Earlier today, I was [invited to sign the open letter calling for the FSF board to resign](https://rms-open-letter.github.io), [which I did](https://github.com/rms-open-letter/rms-open-letter.github.io/pull/44).  To me, it was obvious to sign the letter, which on it's own makes a compelling argument for [why RMS should not be an executive director at FSF](https://rms-open-letter.github.io/appendix).

But I believe there is an even more compelling reason.

When we started Alpine 15 years ago, we largely copied the way other distributions handled things... and so Alpine copied the problems that many other FOSS projects had as well.  Reviews were harsh and lacking empathy, which caused problems with contributor burnout.  During this time, Alpine had some marginal amount of success, Docker did eventually standardize on Alpine as platform of choice for micro-containers and postmarketOS was launched.

Due to burnout, I turned in my commit privileges, resigned from the core team and wound up taking a sabbatical from the project for almost a year.

In the time I was taking a break from Alpine, an amazing thing happened: the core team decided to change the way things were done in the project.  Instead of giving harsh reviews as the bigger projects did, the project pivoted towards a philosophy of _collaborative kindness_.  And as a result, burnout issues went away, and we started attracting all sorts of new talent.

[Robert Hansen resigned as the GnuPG FAQ maintainer today](https://twitter.com/robertjhansen/status/1374242002653577216).  In his message, he advocates that we should _demand_ kindness from our leaders, and he's right, it gets better results.  Alpine would not be the success it is today had we not decided to change the way the project was managed.  I want that level of success for FOSS as a whole.

Unfortunately, it is proven again and again that RMS is not capable of being kind.

He screams at interns when they do not do work to his exact specification (unfortunately, FSF staff are forced to sign an NDA that covers almost every minutia of their experience being an FSF staffer so this is not easy to corroborate).

He shows up on e-mail lists and overrides the decisions made by his subordinates, despite those decisions having strong community consensus.

He is a wholly ineffective leader, and his continued leadership will ultimately be harmful to FSF.

And so, that is why I signed the letter demanding his resignation yet again.
