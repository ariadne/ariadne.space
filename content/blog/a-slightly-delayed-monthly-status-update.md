---
title: "A slightly-delayed monthly status update"
date: "2021-06-04"
---

A few weeks ago, I announced the [creation of a security response team for Alpine](https://ariadne.space/2021/04/20/building-a-security-response-team-in-alpine/), of which I am presently the chair.

Since then, the team has been fully chartered by both the previous Alpine core team, and the new Alpine council, and we have gotten a few members on board working on security issues in Alpine.  Once the Technical Steering Committee is fully formed, the security team will report to the TSC and fall under its purview.

Accordingly, I thought it would be prudent to start write monthly updates summarizing what I've been up to.  This one is a little delayed because we've been focused on getting Alpine 3.14 out the door (first RC should come out on Monday)!

## secfixes-tracker

One of the primary activities of the security team is to manage the [security database](https://secdb.alpinelinux.org).  This is largely done using the secfixes-tracker application I wrote in April.  At AlpineConf, I gave a bubble talk about the new security team, including a demonstration of how we use the secfixes-tracker application to research and mitigate security vulnerabilities.

Since the creation of the security team through the Alpine 3.14 release cycle, myself and other security team volunteers have mitigated over 100 vulnerabilities through patching or non-maintainer security upgrades in the pending 3.14 release alone and many more in past releases which are still supported.

All of this work in finding unpatched vulnerabilities is done using secfixes-tracker.  However, while it finds many vulnerabilities, it is not perfect.  There are both false positives and false negatives, which we are working on improving.

The next step for secfixes-tracker is to integrate it into GitLab, so that maintainers can log in and reject CVEs they deem irrelevant in their packages instead of having to attribute a security fix to version `0`.  I am also [working on a protocol to allow security trackers to share data](https://docs.google.com/document/d/11-m_aXnrySM6KeA5I6BjdeGeSIxymfip4hseg2Y0UKw/edit#heading=h.bz0hbmpvjhfb) with each other in an automated way.

## Infrastructure

Another role of the security team is to advise the infrastructure team on security-related matters.  In the past few weeks, this primarily focused around two issues: how to [securely relay patches from the alpine-aports mailing list into GitLab without compromising the security of `aports.git`](https://gitlab.alpinelinux.org/mailinglist-bot) and [our response to recent changes in freenode](https://freenode.net/news/freenode-is-foss), where it was the recommendation of the security team to [leave freenode in favor of OFTC](https://alpinelinux.org/posts/Switching-to-OFTC.html).

## Reproducible Builds

Another project of mine personally is working to prove the reproducibility of Alpine package builds, as part of the [Reproducible Builds project](https://reproducible-builds.org/).  To this end, I hope to have the Alpine 3.15 build fully reproducible.  This will require some changes to `abuild` so that it produces buildinfo files, as well as a rebuilder backend.  We plan to use the same buildinfo format as Arch, and will likely adapt some of the other reproducible builds work Arch has done to Alpine.

I plan to have a meeting within the next week or two to formulate an official reproducible builds team inside Alpine and lay out the next steps for what we need to do in order to get things going.  In the meantime, join `#alpine-reproducible` on `irc.oftc.net` if you wish to follow along.

I plan for reproducible builds (perhaps getting all of main reproducible) to be a sprint in July, once the prerequisite infrastructure is in place to support it, so stay tuned on that.

## apk-tools 3

On this front, there's not much to report yet.  My goal is to integrate the security database into our APKINDEX, so that we can have `apk list --upgradable --security`, which lists all of the security fixes you need to apply.  Unfortunately, we are still working to finalize the ADB format which is a prerequisite for providing the security database in ADB format.  It does look like Timo is almost done with this, so once he is done, I will be able to start working on a way to reflect the security database into our APKINDEX files.

## The `linux-distros` list

There is a mailing list which is intended to allow linux distribution security personnel to discuss security issues in private.  As Alpine now has a security team, it is possible for Alpine to take steps to participate on this list.

However... participation on this list comes with a few restrictions: you have to agree to follow all embargo terms in a precise way.  For example, if an embargoed security vulnerability is announced there and the embargo specifies you may not patch your packages until XYZ date, then you must follow that or you will be kicked off the list.

I am not sure it is necessarily appropriate or even valuable for Alpine to participate on the list.  At present, if an embargoed vulnerability falls off a truck and Alpine notices it, we can fix it immediately.  If we join the `linux-distros` list, then we may be put in a position where we have to hide problems, which I didn't sign up for.  I consider it a feature that the Alpine security team is operating fully in the open for everyone to see, and want to preserve that as much as possible.

The other problem is that distributions which participate [bind their package maintainers to an NDA](https://wiki.gentoo.org/wiki/Project:Security/Pre-Release-Disclosure) in order to look at data relevant to their packages.  I don't like this at all and feel that it is not in the spirit of free software to make contributors acknowledge an NDA.

We plan to discuss this over the next week and see if we can reach consensus as a team on what to do.  I prefer to fix vulnerabilities, not wait to fix vulnerabilities, but obviously I am open to being convinced that there is value to Alpine's participation on that list.

## Acknowledgement

My activities relating to Alpine security work are presently sponsored by Google and the Linux Foundation.  Without their support, I would not be able to work on security full time in Alpine, so thanks!
