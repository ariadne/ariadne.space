---
title: "Oracle cloud sucks"
date: "2021-07-14"
coverImage: "Screen-Shot-2021-07-13-at-10.49.59-PM.png"
---

**Update:** Oracle have made this right, and I am in fact, now running [production services on their cloud](https://ariadne.space/2021/07/18/moving-my-blog-to-oracle-cloud/).  Thanks to Ross and the other Oracle engineers who reached out offering assistance.  The rest of the blog post is retained for historical purposes.

In my previous blog, I said that [Oracle was the best option for cheap ARM hosting](https://ariadne.space/2021/07/10/its-time-for-arm-to-embrace-traditional-hosting/).

Yesterday, Oracle rewarded me for that praise by demonstrating they are, in fact, Oracle and terminating my account.  When I contacted their representative, I was told that I was running services on my instance not allowed by their policies (I was running a non-public IRC server that only connected to other IRC servers, and their policies did not discuss IRC at all) and that the termination decision was final.  Accordingly, I can no longer recommend using Oracle's cloud services for anything -- if you use their service, **you are at risk of losing your hosting at any time**, for any reason they choose to invent, regardless of whether you are a paying customer or not.

That leaves us with exactly zero options for cheap ARM hosting.  Hopefully Amazon will bring ARM options to Lightsail soon.
