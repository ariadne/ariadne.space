---
title: "Bits relating to Alpine security initiatives in July"
date: "2021-08-04"
---

Another month has passed, and we've gotten a lot of work done.  No big announcements to make, but lots of incremental progress, bikeshedding and meetings.  We have been laying the ground work for several initiatives in Alpine 3.15, as well as working with other groups to find a path forward on vulnerability information sharing.

## The Technical Steering Committee

By far the biggest news for this update is that the nascent Alpine Core Team has officially split into the Alpine Council and TSC.  I am a member of the TSC, representing the security team.  We have now had two TSC meetings, and the TSC is already working through a couple of issues referred to it by the security team.  There is still a lot to do involving figuring out the workflow of the TSC, but we are making progress.

### `sudo` deprecation

The first issue the security team referred to the TSC is significant: the security team would like to deprecate `sudo` in favor of `doas`.  This is because `doas` is a much simpler codebase: while `sudo` clocks in at 174k lines of code, `doas` only has 2500 lines, including the portability layer.  Since both are SUID programs, it hopefully makes sense that the security team would prefer to support the simpler `doas` over the much more complicated `sudo`.

But the TSC attached [conditions to its decision](https://gitlab.alpinelinux.org/alpine/tsc/-/issues/1):

- `doas` must gain an equivalent to `/etc/sudoers.d`, so that maintainer scripts can make use of it to do things like automatically restart services.  I have been working with [upstream to enable this support](https://github.com/Duncaen/OpenDoas/pull/71), which should be landing in Alpine later this week.
- `cloud-init` must be adapted to support `doas`.  This is because the cloud images do not enable the root user, so there needs to be a tool to allow escalation to root, such as the venerable `sudo -si`.  Hopefully, a plugin for `cloud-init` will be implemented shortly: we have had some discussion with the `cloud-init` maintainer about this already.

I hope other distributions follow in our footsteps and deprecate `sudo` in favor of `doas`.  While we should try to avoid SUID programs where possible, simpler SUID programs are still better than complicated ones like `sudo`.

### A lifecycle for packages in `testing`

While not directly security-related, since the security team does not provide security support for the `testing` repository, I [raised an issue with the TSC to define a deadline for packages in `testing`](https://gitlab.alpinelinux.org/alpine/tsc/-/issues/2), where they must either leave `testing` and go to `community` or `main`, or they must be removed from `testing`.  This is because people contribute packages to `testing`, and then we never hear from those people ever again, while their packages sit in `testing` basically unmaintained.  This creates problems for the security team when we have to do rebuilds due to libraries changing their `soname`.

The TSC is presently expected to take up this issue at the next meeting, in two weeks.  Hopefully, that translates to a deadline for packages to leave `testing`.

## Reproducible Builds

At the moment, we are still focusing on making the installation media reproducible.  On that front, I reviewed the patch by kpcyrd to make `apk index` reproducible.  After a little bit of discussion, this patch was [accepted into apk-tools](https://gitlab.alpinelinux.org/alpine/apk-tools/-/merge_requests/63) and is included in version 2.12.6 and later.

The next big step is still the design and implementation of buildinfo in Alpine.  With apk-tools 3 around the corner, we are debating using structured data in the package itself instead of an actual file on disk to represent the buildinfo data.

Once that particular design discussion is complete, we will work to get it pushed out for the `edge` builders, and start work on a `rebuilderd` backend.

## secfixes-tracker, security database, sharing

In terms of deployed changes, there's not much to talk about here.  Things are moving along with very few problems.

Behind the scenes, however, we are still working with many stakeholders on enabling real-time vulnerability information sharing.  This is, for whatever reason, a topic where the politics are constantly shifting around: for example, we had the [Distributed Weakness Filing project](https://github.com/distributedweaknessfiling/dwflist), which has now died and been replaced with [Universal Vulnerability Identifiers](https://github.com/cloudsecurityalliance/uvi-database).

The people who run UVI want the same thing we want: real-time vulnerability data sharing based on push messaging and JSON-LD, but they are having the same difficulties that I have been having getting everyone on board with the JSON-LD part.  Unfortunately, the JSON-LD part is the most important part of all of this, because it allows everyone to participate: while the new schema proposed by the Go and OSV teams is a significant improvement over CVE v4, the designers specifically considered URIs, and therefore linked data, to be harmful.

However, linked data is really the thing that is going to let us scale up and track _all_ vulnerabilities.  While it is true that domains can go away, it is possible to archive that data with JSON-LD: the implementations allow for substitution of some URI patterns with other ones (such as archive.org).  At the moment, we track around 20,000 to 30,000 CVEs per year, but in reality, there are likely millions of vulnerabilities found per year.  Having a centralized, gate-kept set of vulnerability databases simply does not scale to this reality.

By using linked data, it is possible to simply walk along a JSON document and discover more information about a vulnerability:

"references": \[
  "https://cve.mitre.org/CVE-2021-34481",
  "https://msrc.microsoft.com/CVE-2021-34481",
  ...
\]

Which itself is really helpful.  But where this becomes even more awesome is when you realize that you can reference vulnerability data in _other_ contexts, such as malware databases:

"name": "Win32/Some.Printer.Malware",
"exploitation\_vectors": \[
  "https://msrc.microsoft.com/CVE-2021-34481"
\]

Now the malware researcher can follow the node for CVE-2021-34481 and get structured data back about the Print Spooler CVE, and it's entirely transparent.

I cannot stress enough that this is how it has to be.  We will never get to the point where we are tracking every single vulnerability unless it is an open system like this, built in the same spirit as FOSS, but for data.

Unfortunately, I suspect there is going to be at least another round of discussions before we get there.  Once these issues are resolved, and a clear path forward is obvious, we will release secfixes-tracker 0.4, which makes the security data available in the standardized JSON-LD format discussed above.

## apk-tools 3

Timo continues to make progress on apk-tools 3.  It is quite possible that Alpine 3.15 will ship with apk-tools 3, and that possibility is getting more likely all the time.

Since the last update, we've concluded that the new index format used by apk-tools 3 is reproducible.

I have also talked with Timo about introducing security fix data into the new apk-tools indices, and we are starting to work on a design for that.  This will enable the `apk list --upgradable --security` feature I have talked about a few times now.

I plan on doing a blog post demonstrating reproducibility of both apk-tools 3 indices as well as apk-tools 3 packages in the next week or so.  I have also been working on writing a specification for the ADB format so that other people can write their own parsers for it.  That will be going upstream as a MR shortly.

## CVEs in Alpine software

Part of the responsibility of the security team is to get CVEs from MITRE.  We are considering becoming a CVE Numbering Authority to improve the process of getting CVEs, but the process CNAs use is kind of clunky, so we might not actually do it.  In July, we requested two CVEs for Alpine software or Alpine-specific packaging problems:

- [CVE-2021-36158](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-36158): The `xrdp` package was generating the same public and private keypair for every single installation.  This was fixed by moving the keypair generation into a maintainer script.  Thanks to Leo for fixing it!
- [CVE-2021-36159](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-36159): Samanta Navarro reported a vulnerability in `libfetch` to Alpine and FreeBSD.  We requested a CVE for the Alpine fork, but then FreeBSD decided to use the same CVE for the original libfetch, too.  As a side effect of coordination of this vulnerability, I proposed creating a unified libfetch project where all users can collaborate on its maintenance.

## Acknowledgement

My activities relating to Alpine security work are presently sponsored by Google and the Linux Foundation. Without their support, I would not be able to work on security full time in Alpine, so thanks!  I also appreciate their [recent sponsoring of kpcyrd, an Alpine, Arch, Debian and reproducible-builds.org contributor](https://vulns.xyz/2021/07/monthly-report/)!
