---
title: "On the topic of community management, CoCs, etc."
date: "2021-08-08"
---

Many people may remember that at one point, Alpine [had a rather troubled community](https://lists.alpinelinux.org/~alpine/devel/%3CCA%2BT2pCE2H8Z8ERg5vS3mnr98Ets%2B0-m0aZpp4ZPzQ%2BhuKMPOjA%40mail.gmail.com%3E#%3CCA+T2pCE2H8Z8ERg5vS3mnr98Ets+0-m0aZpp4ZPzQ+huKMPOjA@mail.gmail.com%3E), which to put it diplomatically, resulted in a developer leaving the project.  This was the result of not properly managing the Alpine community as it grew -- had we taken early actions to ensure appropriate moderation and community management, that particular incident would never have happened.

We did ultimately fix this issue and now have a community that tries to be friendly, welcoming and constructive, but it took a lot of work to get there.  As I was one of the main people who did that work, I think it might be helpful to talk about what I've learned through that process.

## Moderation is critical

For large projects like Alpine, active moderation is the most crucial aspect.  It is basically the part that makes or breaks everything else you try to do.  Building the right moderation team is also important: it needs to be a team that everyone can believe in.

That means that the people who are pushing for community management may or may not be the right people to do the actual day to day moderation work, and should rather focus on policy.  This is because there will be bias against the people pushing for changes in the way the community is managed by some members.  Building a moderation team that gently enforces established policy, but is otherwise perceived as neutral is critical to success.

## Policy statements (such as Codes of Conduct)

It is not necessarily a requirement to write a Code of Conduct.  However, if you are retrofitting one into a pre-existing community, it needs to be done from the bottom up, allowing everyone to say their thoughts.  Yes, you will get people who present bad faith arguments, because they are resistant to change, or perhaps they see no problem with the status quo.  In most cases, however, it is likely because people are resistant to change.  By including the community in the discussion about its community management goals, you ensure they will generally believe in the governance decisions made.

Alpine did ultimately adopt a Code of Conduct.  Most people have never read it, and it doesn't matter.  When we wrote it, we were writing it to address specific patterns of behavior we wanted to remove from the community space.  The real purpose of a Code of Conduct is simply to set expectations, both from participants _and_ the moderation team.

However, if you _do_ adopt a Code of Conduct, you must actually enforce it as needed, which brings us back to moderation.  I have unfortunately seen many projects in the past few years, which have simply clicked the "Add CoC" button on GitHub and attached a copy of the Contributor Covenant, and then went on to do exactly nothing to actually align their community with the Code of Conduct they published.  Simply publishing a Code of Conduct is an optional first step to improving community relations, but it is _never_ the last step.

## Fostering inclusivity

The other key part of building a healthy community is to build a community where everyone feels like they are represented.  This is achieved by encouraging community participation in governance, both at large, and in a targeted way: the people making the decisions and moderating the community should ideally look like the people who actually use the software created.

This means that you should try to encourage women, people of color and other marginalized people to participate in project governance.  One way of doing so is by amplifying their work in your project.  You should also amplify the work of other contributors, too.  Basically, if people are doing cool stuff, the community team should make everyone aware of it.  A great side effect of a community team actively doing this is that it encourages people to work together constructively, which reinforces the community management goals.

## Final thoughts

Although it was not easy, Alpine ultimately implemented all of the above, and the community is much healthier than it was even a few years ago.  People are happy, code is being written, and we're making progress on substantive improvements to the Alpine system, as a community.

Change is scary, but in the long run, I think everyone in the Alpine community agrees by now that it was worth it.  Hopefully other communities will find this advice helpful, too.
