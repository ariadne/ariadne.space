---
title: Free software grows as a function of social utility
date: '2022-08-06'
---

A frequent complaint I see from users and inexperienced contributors
concerning free software projects is that they are allegedly not doing
enough to grow the userbase, sometimes even asserting that a fork is
necessary to right the course of the project.

Are these complaints missing the point, or do they have merit?
How do free software projects grow their userbase into thriving
communities?

In general, these complaints go something like this:

> [PROJECT] developers have explicitly said they do not want the
> project to grow.  The [PROJECT] is its own worst enemy, and this
> is just the latest example of it I've seen.  I don't trust the
> direction of [PROJECT], and neither should you.

The experienced maintainer understands that we must play the long game,
not the short game.  Tactics such as *embrace, extend, extinguish*
are largely only effective when maintainers are looking at the short
term picture.  This is because organic growth in the use of a free
software package is a function of that package's social utility: a
software package which provides utility to its community will experience
growth in adoption because its users will recommend that others try
the software package and join the project's community.

The social utility of a given software package is not necessarily
tied to mass-market adoption.  It is possible for a software package
to be extremely popular in a tight-knit community, while holding
very little social utility for the mass-market, and this is totally
fine.  In fact, this is the case for most software packages which
exist in the world.

Likewise, it is possible to pursue new feature development as a
gamble on obtaining mass-market adoption, and destroy the social
utility of the product for its current userbase.  An experienced
maintainer will recognize that such gambles rarely pay off, and
usually wind up damaging the project rather than growing it.

Unfortunately, society teaches us that we should grow at any cost,
which means that inexperienced maintainers can be swayed by such
arguments to make harmful decisions to their project.  But if we
recognize that these types of arguments are inherently defective,
we can help maintainers to avoid taking them seriously.
