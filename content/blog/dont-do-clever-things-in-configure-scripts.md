---
title: "don't do clever things in configure scripts"
date: "2021-10-25"
coverImage: "Screen-Shot-2021-10-25-at-2.38.18-AM.png"
---

Recently, a new version of ncurses was released and pushed to Alpine. The maintainer of ncurses in Alpine successfully built it on his machine, so he pushed it to the builders, expecting it to build fine on them. Of course, it promptly failed to build from source on the builders, because `make install` did not install the pkg-config `.pc` files to the right location.

You might think, what a weird regression, and you'd be right. After all, pkg-config files are usually just installed to `$libdir/pkgconfig` in any sort of autotools-based build system. Indeed, in the past, this is what ncurses did as well. However, this was clearly too robust of a solution to the problem of determining where to install the pkg-config files, and so innovation™️ happened:

![](/images/Screen-Shot-2021-10-25-at-2.57.06-AM-860x1024.png)

Yes, you are reading this right: it is **scraping debug output** to find the pkg-config search path. This kind of behavior is clever, and it should absolutely be avoided in anything designed to be portable, such as a configure script. In fact, really, a build system should follow established expectations, specifically, in the autotools case, it should just default to `$libdir/pkgconfig` and allow the end user to override that if it makes sense to. Doing anything else is going to lead to edge cases and problems, since it is more clever than the user expects it to be.

## the portable way to query the search list

If you _really_ need to query the pkg-config search list, use `pkg-config --variable=pc_path pkg-config`. It is supported by every implementation of pkg-config, and it is an interface we all agreed to support explicitly for this use case. This method is supported by freedesktop.org's implementation, OpenBSD's implementation and pkgconf, which are the three main implementations in production use.

There is absolutely no need to scrape debug output, which is absolutely subject to change and should _never_ be considered a stable interface in _any_ program.
