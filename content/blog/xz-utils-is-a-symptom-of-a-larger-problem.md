---
title: The XZ Utils backdoor is a symptom of a larger problem
date: 2024-04-02
---

On March 29th, Andres Freund [dropped a bombshell on the oss-security mailing list][0]: recent XZ Utils source code tarball releases made by Jia Tan were released with a backdoor.
Thankfully, for multiple reasons, [Alpine was not impacted by this backdoor][1], despite the recent source code tarball releases being published in Alpine `edge`.
But what lessons do we need to learn from this incident?

  [0]: https://www.openwall.com/lists/oss-security/2024/03/29/4
  [1]: https://alpinelinux.org/posts/XZ-backdoor-CVE-2024-3094.html

## The software "supply chain" is not real

As a community of hackers, we have built an exhaustive commons of free software released under various free licenses such as the GPL and the Apache 2.0 license.
Software packages in this commons have taken over the corporate world, because it enabled more rapid innovation by allowing developers to focus more on the business logic of their applications, rather than low-level details.
This has been overall a good thing for society: from the open commons we have spawned a whole world of applications which have become the foundational bedrock of modern society.
It can certainly be argued that the invention of FOSS licensing models has been as revolutionary for the digital economy as the steam engine was for industry.

There is one problem, however -- when we take software from the commons, we are like raccoons digging through a dumpster to find something useful.
There is no "supply chain" in reality, *but* there is an effort by corporations which consume software from the commons to pretend there is one in order to shift the obligations related to ingesting third-party code away from themselves and to the original authors and maintainers of the code they are using.

For there to be a "supply chain", there must be a supplier, which in return requires a contractual relationship between two parties.
With software licensed under FOSS licensing terms, a consumer receives a non-exclusive license to make use of the software however they wish (in accordance with the license requirements, of course), but non-exclusive licenses cannot and do not imply a contractual supplier-consumer relationship.

With that said, many of the proposals made by people working to improve security of the software "supply chain" have practical and valuable uses for protecting the integrity of the commons, and are worthy of further examination.

## "Junk drawer" libraries are valuable targets

CVE-2024-3094 happened for a simple reason: [distributions patching OpenSSH to support systemd's readiness notifications][2].

  [2]: https://bugs.debian.org/778913

Frequently, authors looking to add systemd readiness notifications to their software tend to just look for the `systemd` pkg-config package, and use its `CFLAGS` and `LIBS`.
This results in the software linking to `libsystemd`.
What does *that* have to do with anything, after all we are talking about a backdoor in `liblzma`, not `libsystemd`?
Simple: although `sd_daemon()` does not make use of any functionality in `liblzma`, because `libsystemd` has a `DT_DEPEND` entry against `liblzma`, it will pull in `liblzma` as a shared object dependency.
Once that happens, the *constructor* functions in `liblzma` will run, as it is being loaded due to being in the dependency graph.

What can be done about this?
A simple solution would be to start to split up the various `libsystemd` routines into smaller packages.
This would allow for these packages to link against `libsystemd-daemon` or similar instead, which would presumably not link against `liblzma`, as it is unnecessary for readiness notifications.
The `systemd` pkg-config package could be kept around as a metapackage pulling in the other libraries as a migration path.

I call libraries which are large amalgamations of unrelated routines "junk drawer" libraries because they are basically the programming equivalent to a junk drawer: routines and dependencies accumulate over years and suddenly you have a mess of programs which depend on this library but only use some small portion of the library.
As these unnecessary dependencies accumulate, these "junk drawer" libraries become valuable points of interest when scouting for projects to compromise.
I would recommend auditing any of the other dependencies of systemd for possible backdoors for this reason.
There are a number of other libraries which could have been targeted in this way as well, which are also in the libsystemd dependency graph, such as PCRE.

## Be kind to software maintainers

Although I am not certain that this lesson is particularly applicable to the xz-utils situation, since the actor who implemented the backdoor most likely made use of sockpuppet personas to advocate for his becoming a maintainer, the mental health of software maintainers is important.

Directly what this means is that if you see somebody harassing a maintainer with specific demands, you should not join in on the thread.
Let the maintainer deal with it publicly, and reach out privately if you are concerned about the situation.
Otherwise, even if you are concerned about burnout or the maintainer overworking, you may wind up advocating for a threat actor to become a maintainer of something.
