---
title: "The Case For Blind Key Rotation"
date: "2018-12-30"
---

ActivityPub uses cryptographic signatures, mainly for the purpose of authenticating messages. This is largely for the purpose of spoofing prevention, but as any observant person would understand, digital signatures carry strong forensic value.

Unfortunately, while ActivityPub uses cryptographic signatures, the types of cryptographic signatures to use have been left unspecified. This has lead to various implementations having to choose on their own which signature types to use.

The fediverse has settled on using not one but _two_ types of cryptographic signature:

- **HTTP Signatures**: based on an [IETF internet-draft](https://tools.ietf.org/html/draft-cavage-http-signatures-10), HTTP signatures provide a cryptographic validation of the headers, including a Digest header which provides some information about the underlying object. HTTP Signatures are an example of _detached_ signatures. HTTP Signatures also generally sign the Date header which provides a defacto validity period.
- **JSON-LD Linked Data Signatures**: based on a [W3C community draft](https://w3c-dvcg.github.io/ld-signatures/), JSON-LD Linked Data Signatures provide an inline cryptographic validation of the JSON-LD document being signed. JSON-LD Linked Data Signatures are commonly referred to as LDS signatures or LDSigs because frankly the title of the spec is a mouthful. LDSigs are an example of _inline_ signatures.

## Signatures and Deniability

When we refer to _deniability_, what we're talking about is _forensic deniability_, or put simply the ability to plausibly argue in a court or tribunal that you did not sign a given object. In essence, _forensic deniability_ is the ability to argue _plausible deniability_ when presented with a piece of forensic evidence.

Digital signatures are by their very nature harmful with regard to _forensic deniability_ because they are digital evidence showing that you signed something. But not all signature schemes are made equal, some are less harmful to deniability than others.

A good signature scheme which does not harm deniability has the following basic attributes:

- Signatures are ephemeral: they only hold validity for a given time period.
- Signatures are revocable: they can be invalidated during the validity period in some way.

Both HTTP Signatures and LDSigs have weaknesses — specifically, both implementations do not allow for the possibility of future revocation of the signature, but LDSigs is even worse because LDSigs are intentionally forever.

## Mitigating the revocability problem with Blind Key Rotation

Blind Key Rotation is a mitigation that builds on the fact that ActivityPub implementations must fetch a given actor again in the event that signature authentication fails, by using this fact to provide some level of revocability.

The mitigation works as follows:

1. You delete one or more objects in a short time period.
2. Some time after the deletions are processed, the instance rekeys your account. It does not send any Update message or similar because signing your new key with your old key defeats the purpose of this exercise.
3. When you next publish content, signature validation fails and the instance fetches your account's actor object again to learn the new keys.
4. With the new keys, signature validation passes and your new content is published.

It is important to emphasize that in a Blind Key Rotation, you do not send out an Update message with new keys. The reason why this is, is because you do not want to create a cryptographic relationship between the keys. By creating a cryptographic relationship, you introduce new digital evidence which can be used to prove that you held the original keypair at some time in the past.

## Questions?

If you still have questions, contact me on the fediverse: [@kaniini@pleroma.site](https://pleroma.site/kaniini)
