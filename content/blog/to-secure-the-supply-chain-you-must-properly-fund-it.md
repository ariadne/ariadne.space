---
title: "to secure the supply chain, you must properly fund it"
date: "2021-12-11"
---

Yesterday, a new [0day vulnerability dropped in Apache Log4j](https://nvd.nist.gov/vuln/detail/CVE-2021-44228). It turned out to be worse than the initial analysis: because of recursive nesting of substitutions, [it is possible to execute remote code in any program which passes user data to Log4j for logging](https://twitter.com/_StaticFlow_/status/1469358229767475205?s=20). Needless to say, the way this disclosure was handled was a disaster, as it was quickly discovered that many popular services were using Log4j, but how did we get here?

Like many projects, Log4j is only maintained by volunteers, and because of this, coordination of security response is naturally more difficult: a coordinated embargo is easy to coordinate, if you have a dedicated maintainer to do it. In the absence of a dedicated maintainer, you have chaos: as soon as a commit lands in git to fix a bug, the race is on: security maintainers are scurrying to reverse engineer what the bug you fixed was, which is why vulnerability embargoes can be helpful.

It turns out that like many other software projects in the commons, Log4j does not have a dedicated maintainer, while corporations make heavy use of the project, and so, as usual, the maintainers have to beg for scraps from their fellow peers or the corporations that use the code. Incidentally, [one of the Log4j maintainers' GitHub sponsors profile is here](https://github.com/sponsors/rgoers), if you would like to contribute some money to his cause.

When corporations sponsor the maintenance of the FOSS projects they use, they are effectively buying an insurance policy that guarantees a prompt, well-coordinated response to security problems. The newly established Open Source Program Offices at these companies should ponder which is more expensive: $100k/year salary for a maintainer of a project they are heavily dependent upon, or millions in damages from data breaches when a security vulnerability causes serious customer data exposure, like this one.
