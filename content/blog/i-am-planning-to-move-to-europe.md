---
title: "I am planning to move to Europe"
date: "2021-09-02"
---

I have been considering a move to Europe since the 2018 midterm election, though a combination of friends being persuasive and the COVID-19 pandemic put a damper on those plans.  Accordingly, I have tried my best to give Biden and the democrats an opportunity to show even the most basic modicum of progress on putting the country on a different path.  I did my part, I held my nose and as I was told, voted blue no matter who, despite a total lack of enthusiasm for the candidates.  But honestly, I can't do this anymore.

Yesterday, Texas' SB 8 went into force of law.  This is a law which [incentivizes citizens to bring litigation against anybody involved in administering an abortion](https://en.wikipedia.org/wiki/Texas_Heartbeat_Act) to anybody who is six weeks pregnant or later, by issuing a $10,000 reward paid by the state for such litigation if successful.

As I do not have a uterus, I am unable to get pregnant, and so you might wonder why I care about this so much.  It is simple: the right to bodily autonomy should matter to everyone.  However, there is a second reason as well.  The US Supreme Court, when requested to review the law, [determined there was no constitutionality issue with it](https://www.supremecourt.gov/opinions/20pdf/21a24_8759.pdf), because of the financial incentive system implemented by the law.

This means that the legislators in Texas have found a vulnerability in our legal system, one which will be surely abused to institute other draconian policies driven by the religious right.  The people who are implementing this strategy will do their best to outlaw _any_ form of contraception.  They will try their best to outlaw queer relationships.  And, most relevant to me, they will use this vulnerability against trans people to make even our basic existence illegal.

My confidence in the continued viability for my safety in the US began to wane earlier this summer, when a transphobic activist invented false allegations that a transgender woman flashed her genitalia at other women at Wi Spa in Los Angeles.  These allegations resulted in [violent demonstrations by white supremacists](https://www.theguardian.com/world/2021/jul/28/anti-trans-video-los-angeles-protest-wi-spa).  Don't worry, the message was received.

This isn't strictly about safety, however.  I also recognize that leaving the US is a selfish choice, and that I have a lot of privilege that others may not have.  But the thing is, I've worked my tail off to get where I am, on my own.

As a skilled tax-paying worker, I believe the US has an obligation to respect me as a person, if they wish for me to retain my patronage.  In other words, they should be competing for myself and other skilled labor to remain.  Instead, the lack of any tangible action to bring an end to Trumpism and lack of any legislative resistance to trans people being the next stop in the culture war, shows that I am not wanted here, and so I will move somewhere else, where I will be safe and respected as a person.

And so I plan to move to the Netherlands within the next year.  You see, as I primarily work as a contractor, I have a business vehicle.  The [Dutch American Friendship Treaty](https://en.wikipedia.org/wiki/DAFT) allows me to very easily acquire a permanent Schengen visa, and after some time, citizenship, simply by moving my business vehicle to the Netherlands.  Additionally, I already have many friends in Europe, so making new friends is not something I have to worry a lot about.

I'm not sure how moving my business vehicle to Europe will affect any current engagements, so in the interest of redundancy, I will be putting together a CV package and putting together a website for my business vehicle within the next few weeks.  If you have a European company that could benefit from my unique experiences and insight, feel free to reach out.  Similarly, I'll probably be looking for some sort of housing arrangements in the next few months, so suggestions in that area are welcome too.
