---
title: "It's time for ARM to embrace traditional hosting"
date: "2021-07-10"
---

ARM is everywhere these days -- from phones to hyperscale server deployments.  There is even an [ARM workstation available that has decent specs at an acceptable price](https://www.solid-run.com/arm-servers-networking-platforms/honeycomb-workstation/).  Amazon and Oracle tout white paper after white paper about how their customers have switched to ARM, gotten performance wins and saved money.  Sounds like everything is on the right track, yes?  Well, actually it's not.

## ARM for the classes, x86 for the masses

For various reasons, I've been informed that I need to start rethinking my server infrastructure arrangements.  We won't go into that here, but the recent swearing at San Francisco property developers on my Twitter is highly related.

As I am highly allergic to using any infrastructure powered by x86 CPUs, due to the fact that [Intel and AMD both include firmware in the CPU which allow for computation to occur without my consent](https://en.wikipedia.org/wiki/Intel_Management_Engine) (also known as a backdoor) so that Hollywood can implement a largely pointless (especially on a server) digital restrictions management scheme, I decided to look at cloud-based hosting solutions using ARM CPUs, as that seemed perfectly reasonable at first glance.

Unfortunately, what I found is that ARM hosting is not deployed in a way where individual users can access it at cost-competitive prices.

## AWS Graviton (bespoke Neoverse CPUs)

In late 2018, AWS announced the [Graviton CPU, which was based on a core design they got when they acquired Annapurna Labs](https://aws.amazon.com/blogs/aws/new-ec2-instances-a1-powered-by-arm-based-aws-graviton-processors/).  This was followed up in 2020 with [Graviton2, which is based on the ARM Neoverse N1 core design](https://www.anandtech.com/show/15578/cloud-clash-amazon-graviton2-arm-against-intel-and-amd).  These are decent chips, the performance is quite good, and costs are quite low.

But, how much does it cost for an average person to actually make use of it?  We will assume that the 1 vCPU / 4GB RAM `m6g.medium` configuration is suitable for this comparison, as it is the most comparable to a modest x86 VPS.

The `m6g.medium` instance does not come with any transfer, but the first GB is always free on EC2.  Further transfer is $0.09/GB up to 10TB.  By comparison, the Linode 4GB RAM plan comes with 4TB of transfer, so we will use that for our comparison.

<table><tbody><tr><th>Hourly price (<code>m6g.medium</code>)</th><td>$0.0385</td></tr><tr><th>× 720 hours</th><td>$27.72</td></tr><tr><th>+ 3.999TB of transfer ($0.09 × 3999)</th><td>$359.90</td></tr><tr><th>Total:</th><td>$387.62</td></tr></tbody></table>

Transfer charges aside, the $27.72 monthly charge is quite competitive to Linode, clocking in at only $7.72 more expensive for comparable performance.  But the data transfer charges have the potential to make using Graviton on EC2 very costly.

### What about AWS Lightsail?

An astute reader might note that AWS actually _does_ provide traditional VPS hosting as a product, under its [Lightsail brand](https://aws.amazon.com/lightsail/).  But the Lightsail VPS product is x86-only for now.

Amazon could make a huge impact in terms of driving ARM adoption in the hosting ecosystem by bringing Graviton to their Lightsail product.  Capturing Lightsail users into the Graviton ecosystem and then scaling them up to EC2 seems like a no-brainer sales strategy too.  But so far, they haven't implemented this.

## Oracle Cloud Infrastructure

A few months ago, [Oracle introduced instances based on Ampere's Altra CPUs](https://blogs.oracle.com/cloud-infrastructure/post/arm-based-cloud-computing-is-the-next-big-thing-introducing-arm-on-oracle-cloud-infrastructure), which are also based on the Neoverse N1 core.

The base configuration (Oracle calls it a shape) is priced at $0.01/hourly, includes a single vCPU and 6GB of memory.  These instances do not come with any data transfer inclusive, but like AWS, data transfer is pooled.  A major difference between Oracle and AWS, however, is that the first 10TB of transfer is included gratis.

<table><tbody><tr><th>Hourly price</th><td>$0.01</td></tr><tr><th>× 720 hours</th><td>$7.20</td></tr><tr><th>+ 4TB transfer (included gratis)</th><td>$0</td></tr><tr><th>Total:</th><td>$7.20</td></tr></tbody></table>

I really, _really_ wanted to find a reason to hate on Oracle here.  I mean, they are Oracle.  But I have to admit that Oracle's cloud product is a lot more similar to traditional VPS hosting than Amazon's EC2 offerings.  **Update:** Haha, nevermind!  They came up with a reason for me to hate on them when they [terminated my account for no reason](https://ariadne.space/2021/07/14/oracle-cloud-sucks/).

So, we have one option for a paid ARM VPS, and that is only an option if you are willing to deal with Oracle, which are Oracle.  Did I mention they are Oracle?

![Oracle federating its login service with itself](/images/Screen-Shot-2021-07-09-at-9.14.41-PM-300x189.png)

## Scaleway

Tons of people told me that Scaleway had ARM VPS for a long time.  And indeed, they used to, but they don't anymore.  Back when they launched ARMv8 VPS on ThunderX servers, I actually used a Scaleway VPS to port `libucontext`.

Unfortunately, they no longer offer ARM VPS of any kind, and only overpriced x86 ones that are not remotely cost competitive to anything else on that market.

## Mythic Beasts, miniNodes, etc.

These companies offer ARM instances, but they are Raspberry Pi instances.  The pricing is also rather expensive when considering that they are Raspberry Pi instances.  I don't consider these offers competitive in any way.

## Equinix Metal

You can still buy ARM servers on the Equinix Metal platform, but [you have to request permission to buy them](https://metal.equinix.com/developers/docs/servers/server-specs/#arm-servers).  In testing a couple of years ago, I was able to provision a `c1.large.arm` server on the spot market for $0.25/hour, which translates to $180/monthly.

However, the problem with buying on the spot market is that your server might go away at any time, which means you can't actually depend on it.

There is also the problem with data transfer: Equinix Metal follows the same billing practices for data transfer as AWS, meaning actual data transfer gets expensive quickly.

However, the folks who run Equinix Metal are great people, and I feel like ARM could work with them to get some sort of side project going where they get ARM servers into the hands of developers at reasonable pricing.  They already have an arrangement like that for FOSS projects with the Works on ARM program.

## Conclusions

Right now, as noted above, Oracle is the best game in town for the average person (like me) to buy an ARM VPS.  We need more options.  Amazon should make Graviton available on its Lightsail platform.

It is also possible that as a side effect of marcan's [Asahi Linux project](https://asahilinux.org/), we might have cheap Linux dedicated servers on Apple M1 mac minis soon.  That's also a space to watch.
