---
title: "GNU nano is my editor of choice"
date: "2021-08-13"
---

I have been using GNU nano for the overwhelming majority of my life.  Like an old friend, `nano` has always been reliable and has never failed me where other text editors have.  By far, it has been the most influential software I have ever used regarding how I approach the design of my own software.

## The `vim` vs `emacs` flame war

I've used both `vim` and `emacs`.  I don't like either of them, for differing reasons: modal editing doesn't really fit my mental model of how an editor should work, and Emacs Lisp is not really a particularly fun language to use simply for customizing the behavior of an editor -- as they say, Emacs is a nice operating system, it just needs a good editor.

In all cases, I think `nano` is a much better editor (at least for me), as when properly configured (and previously with some patching), it provides all of the features from `vim` I would want anyway, but in a modeless format.

## A note about `pico`

As most people know, GNU nano began its life as a clone of UW pico.  Pico (short for PIne COmposer) was bundled with the PINE email client, which was written by the University of Washington.  Unfortunately, PINE was distributed under a custom license which had many problems.  This was eventually solved when the University of Washington released ALPINE (short for Alternatively Licensed PINE) under the Apache 2.0 license.

The licensing problem in combination with a desire to make a more capable editor based on the overall `pico` user experience led to the creation of GNU nano.

In the Alpine Linux distribution, both `pico` and `nano` are available.  Here's what `pico` looks like by default:![A screenshot of Pico showing some code, in its default configuation. The help options and titlebar are present.](/images/Screenshot_20210813_131025.png)

## The default `nano` experience

Like with `pico`, the default UI for `nano` is quite boring to look at.  Here is GNU nano displaying the same file with the default configuration:

![The GNU nano editor showing some code in its default configuration. The help bar highlights undo/redo support and other features not present in Pico.](/images/Screenshot_20210813_131745.png)As you can hopefully see, the default `nano` configuration is quite similar to that of `pico`.  However, unlike `pico`, it can be changed by editing the `~/.nanorc` file.

## Building something like `vim` using `.nanorc`

What I want in an editor is something that basically looks like `vim`, but is modeless like `nano`.  Something like this:

![GNU nano displaying source code as I have configured it, syntax highlighting is enabled, and minibar mode also.](/images/Screenshot_20210813_132423.png)But how do we get there?  The answer is simple: we use the `~/.nanorc` file.

![GNU nano displaying my .nanorc file. Some features are enabled, and some syntax highlighting packages are included.](/images/Screenshot_20210813_132854.png)As a result of many people wanting the same thing: `vim`\-like functionality with modeless editing, `nano` gained several third-party patches which allowed for this.  For the most part, these patches (or equivalent functionality) have been included upstream in recent years.

Getting most of the way to a vim-like look and feel, without syntax highlighting, is quite simple.  You simply need to add these lines to your `~/.nanorc` file with any recent version of `nano`:

\# enables the minibar feature
set minibar

# disables the shortcut hints
set nohelp

That gets you something like this:

![GNU nano with minibar and help disabled.](/images/Screenshot_20210813_133705.png)

However, that minibar looks a little ugly with the inverse text.  The good news is, we can disable the inverse text by adding another snippet to `~/.nanorc`:

\# disable inverse text for the minibar
set titlecolor normal,normal

The way this works is by setting the foreground and background colors for the titlebar to `normal`, which means that `nano` shouldn't change whatever color is already set.  That gives us:

![GNU nano with minibar enabled, help disabled, and titlecolor set to normal/normal.](/images/Screenshot_20210813_134130.png)

## Enabling syntax highlighting

There are two ways that syntax highlighting can be enabled in `nano`: both come down to including configuration snippets to enable it.  GNU nano comes with some sample syntax highlighting configuration, which on Alpine systems is available in the `nano-syntax` package, but I don't personally use it, as the color scheme is quite ugly.

Instead, I use an improved [syntax highlighting package that is distributed on GitHub](https://github.com/scopatz/nanorc).  To install it, you can just do something like:

nanabozho:~$ git clone git@github.com:scopatz/nanorc ~/.nano/
\[...\]

This will install the syntax highlighting package to `~/.nano`.  At that point, you just add `include` lines for the syntax highlighters you want to enable:

include "~/.nano/c.nanorc"

Once you do that, you're done and left with a nano that looks like this:

![GNU nano displaying source code as I have configured it, syntax highlighting is enabled, and minibar mode also.](/images/Screenshot_20210813_132423.png)

Hopefully this post demonstrates that `nano` is a quite capable editor in its own right.
