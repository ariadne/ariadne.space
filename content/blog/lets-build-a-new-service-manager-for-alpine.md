---
title: "Let's build a new service manager for Alpine!"
date: "2021-03-25"
---

**Update (April 27)**: Please visit Laurent's website on this issue [for a more detailed proposal](https://skarnet.com/projects/service-manager.html).  If you work at a company which has budget for this, please get in touch with him directly.

As many of you already know, Alpine presently uses an fairly [modified version of OpenRC](https://git.alpinelinux.org/aports/tree/main/openrc) as its service manager.  Unfortunately, OpenRC maintenance has stagnated: the last release was over a year ago.

We feel now is a good time to start working on a replacement service manager based on user feedback and design discussions we've had over the past few years which can be simply summarized as _systemd done right_.  But what does _systemd done right_ mean?

Our plan is to build a supervision-first service manager that consumes and reacts to events, using declarative unit files similar to systemd, so that administrators who are familiar with systemd can easily learn the new system.  In order to build this system, we plan to work with [Laurent Bercot](http://skarnet.org/), a globally recognized domain expert on process supervision systems and author of the [s6 software supervision suite](https://skarnet.org/software/s6/).

This work will also build on the work we've done with ifupdown-ng, as ifupdown-ng will be able to reflect its own state into the service manager allowing it to start services or stop them as the network state changes.  OpenRC does not support reacting to arbitrary events, which is why this functionality is not yet available.

Corporate funding of this effort would have meaningful impact on the timeline that this work can be delivered in.  It really comes down to giving Laurent the ability to work on this full time until it is done.  If he can do that, like Lennart was able to, he should be able to build the basic system in a few months.

Users outside the Alpine ecosystem will also benefit from this work.  Our plan is to introduce a true contender to systemd that is completely competitive as a service manager.  If you believe real competition to systemd will be beneficial toward driving innovation in systemd, you should also want to sponsor this work.

Alpine has gotten a lot of mileage out of OpenRC, and we are open to contributing to its future maintenance while Alpine releases still include it as part of the base system, but our long-term goal is to adopt the s6-based solution.

If you're interested in sponsoring Laurent's work on this project, you can contact him [via e-mail](ska-remove-this-if-you-are-not-a-bot@skarnet.org) or via [his Twitter account](https://twitter.com/laurentbercot).
