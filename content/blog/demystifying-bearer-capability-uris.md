---
title: "Demystifying Bearer Capability URIs"
date: "2019-10-11"
---

Historically, there has been a practice of combining URIs with access tokens containing sufficient entropy to make them difficult to brute force. A few different techniques have been implemented to do this, but those techniques can be considered implementation specific. One of the earliest and most notable uses of this technique [can be observed in the Second Life backend APIs](http://wiki.secondlife.com/wiki/Linden_Lab_Official:Registration_API#Using_capability_URLs).

An example of a capability URL being used in the fediverse today would be in the way that Pleroma refers to ActivityPub objects and activities: `https://socially.whimsic.al/objects/b05e883b-b66f-421b-ac46-c6018f539533` is a capability URL that allows access to a specific object.

However, while it is difficult for capability URLs to be brute forced due to containing tokens that have sufficient entropy to make guessing them expensive, they are have a serious flaw — since the access token is _part of the resource being accessed_, they tend to leak the access token in two ways:

- Browser `Referer` headers
- Server access and error logs

Since the usage of capability URLs has become widespread, a wonderful specification has been released by the IETF: OAuth 2.0. OAuth provides a possible solution in the form of _bearer tokens_, which can be passed around using an `Authorization` header.

Bearer Capability URIs (aka bearcaps) are essentially a construct which combines a resource with an access token that happens to grant some form of access to that resource, while keeping that access token out of band. Since the access token is out of band, it cannot be leaked in a `Referer` header or in a server log.

But what does a bearcap URI look like? It's actually quite simple. Let's build on the example URL I gave above — it's `bearcap` URI would be something like: `bearcap:?u=https://socially.whimsic.al/notice/9nnmWVszgTY13FduAS&t=b05e883b-b66f-421b-ac46-c6018f539533`.

Let's break this down into parts.

`bearcap` is the name of the URI scheme, and then that is further broken down into a pair of URI query parameters:

- `u` is the URI that the supplied token happens to be able to access in some way
- `t` is the access token itself

In this case, a client which wanted to consume this bearcap (say to fetch the object behind it) would make a `GET` request to the URI specified, with the token specified:

```
GET /notice/9nnmWVszgTY13FduAS HTTP/1.1
Host: socially.whimsic.al
Authorization: Bearer b05e883b-b66f-421b-ac46-c6018f539533

HTTP/1.1 200 OK
[...]
```

In a future post, we will go into how bearcap URIs can help to bring some aspects of capability-based security to ActivityPub.
