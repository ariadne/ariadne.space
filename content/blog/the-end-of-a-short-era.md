---
title: "The End of a Short Era"
date: "2021-03-21"
coverImage: "Screenshot_2021-02-13-jejune-client.png"
---

Earlier this year, I started [a project called Jejune](https://github.com/kaniini/jejune) and migrated my blog to it.  For various reasons, I have decided to switch to WordPress instead.

The main reason why is because WordPress has plugins which do everything I wanted Jejune to do, so using an already established platform provides more time for me to work on my more important projects.

For posting to the fediverse, I plan to use a public Mastodon or Pleroma instance, though most of my social graph migrated back to Twitter so I probably won't be too active there.  After all, my main reason for using social platforms is to communicate with my friends, so I am going to be where my friends actually are.  Feel free to let me know suggestions, though!
