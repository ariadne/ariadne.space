---
title: "you can't stop the (corporate) music"
date: "2021-09-28"
---

I've frequently said that marketing departments are the most damaging appendage of any modern corporation. However, there is one example of this which really proves the point: corporate songs, and more recently, corporate music videos. These Lovecraftian horrors are usually created in order to raise employee morale, typically at the cost of hundreds of thousands of dollars and thousands of man-hours being wasted on meetings to compose the song by committee. But don't take my word for it: here's some examples.

## HP's "Power Shift"

https://www.youtube.com/watch?v=VLTh4uVJduI

With a corporate song like this, it's no surprise that PA-RISC went nowhere.

Lets say you're a middle manager at Hewlett-Packard in 1991 leading the PA-RISC workstation team. Would you wake up one day and say "I know! What we need is to produce a rap video to show why PA-RISC is cool"? No, you probably wouldn't. But that's what somebody at HP did. The only thing this song makes me want to do is **not** buy a PA-RISC workstation. The lyrics likely haunt the hired composer to this day, just look at the hook:

You want power,
you want speed,
the 700 series,
is what you need!

PA-RISC has set the pace,
Hewlett-Packard now leads the race!

## Brocade One: A Hole in None

https://www.youtube.com/watch?v=RzAnyfgpUcE

This music video is so bad that Broadcom acquired Brocade to put it out of its misery a year later.

Your company is tanking because Cisco and Juniper released new products, such as the Cisco Nexus and Juniper MX router, that were far better than the Brocade NetIron MLXe router. What do you do? Make a better router? Nah, that's too obvious. Instead, make a rap video talking about how your management tools are better! (I can speak from experience that Brocade's VCS solution didn't actually work reliably, but who cares about facts.)

## PriceWaterhouseCoopers: talking about taxes, state and local

https://www.youtube.com/watch?v=itWiTKU4nCo

If you ever wondered if accountants could write hard hitting jams: the answer is no.

At least this one sounds like they made it in-house: the synth lead sounds like it is a Casio home synthesizer, and the people singing it sound like they probably worked there. Outside of the completely blasé lyrics, this one is surprisingly tolerable, but one still has to wonder if it was a good use of corporate resources to produce. Most likely not.

## The Fujitsu Corporate Anthem

https://www.youtube.com/watch?v=FRTf3UXCpiE

Fujitsu proves that this isn't just limited to US companies

As far as corporate songs go, this one is actually quite alright. Fujitsu went all out on their propaganda exercise, hiring Mitsuko Miyake to sing their corporate power ballad, backed by a big band orchestra. If you hear it from them, Fujitsu exists to bring us all into a corporate utopia, powered by Fujitsu. Terrifying stuff, honestly.

## The Gazprom Song: the Soviet Corporate Song

https://www.youtube.com/watch?v=xGbI87tyr\_4

Remember when Gazprom had a gas leak sealed by detonating an atomic bomb?

A Russian friend of mine sent me this when I noted I was looking for examples of corporate propaganda. This song is about Gazprom, the Russian state gas company. Amongst other things, it claims to be a national savior in this song. I have no idea if that's true or not, but they once had a gas leak sealed by [getting the Soviet military to detonate an atomic bomb](https://www.youtube.com/watch?v=3kwQfjGnVpw), so that seems pretty close.

## Please stop making these songs

While I appreciate material where the jokes write themselves, these songs represent the worst of corporatism. Spend the money buying employees something they would actually appreciate, like a gift card or something instead of making these eldritch horrors. Dammit, I still have the PWC one stuck in my head. Gaaaaaaah!
