---
title: "the three taps of doom"
date: "2021-07-03"
---

A few years ago, I worked as the CTO of an advertising startup.  At first, we used Skype for messaging amongst the employees, and then later, we switched to Slack.  The main reason for switching to Slack was because they had an IRC gateway -- you could connect to a Slack workspace with an IRC client, which allowed for the people who wanted to use IRC to do so, while providing a polished experience for those who were unfamiliar with IRC.

## the IRC gateway

In the beginning, Slack had an IRC gateway.  On May 15th, 2018, Slack [discontinued the IRC gateway](https://web.archive.org/web/20180314224655/https://get.slack.help/hc/en-us/articles/201727913-Connect-to-Slack-over-IRC-and-XMPP), beginning my descent into [Cocytus](https://en.wikipedia.org/wiki/Cocytus#In_the_Divine_Comedy).  Prior to the shutdown of the IRC gateway, I had always interacted with the Slack workspace via IRC.  This was replaced with the Slack mobile and desktop apps.

The IRC gateway, however, was quite buggy, so it was probably good that they got rid of it.  It did not comply with any reasonable IRC specifications, much less support anything from IRCv3, so the user experience was quite disappointing albeit serviceable.

## the notifications

Switching from IRC to the native Slack clients, I now got to deal with one of Slack's main features: notifications.  If you've ever used slack, you're likely familiar with the [unholy notification sound](https://www.youtube.com/watch?v=U7iGyCdA0xk), or as I have come to know it, the triple tap of existential doom.  Let me explain.

At this point, we used slack for _everything_: chat, paging people, even monitoring tickets coming in.  The workflow was efficient, but due to matters outside my control, revenues were declining.  This lead to the CEO becoming quite antsy.  One day he discovered that he could use `@all`, `@tech` or `@sales` to page people with his complaints.

This means that I would now get pages like:

**Monitoring:** `@tech` Service `rtb-frontend-nyc` is degraded **CEO:** `@tech` I demand you implement a filtering feature our customer is requiring to scale up

The monitoring pages were helpful, the CEO paging us demanding that we implement filtering features that spied on users and definitely would not actually result in scaled up revenue (because the customers were paying CPM) were not helpful.

The pages in question were actually a lot more intense than I show here, these are tame examples, but it felt like I had to walk on eggshells in order to use Slack.

## Quitting that job

In the middle of 2018, I quit that job for various reasons.  And as a result, I uninstalled Slack, and immediately felt much better.  But every time I hear the Slack notification sound, I now get anxious as a result.

The moral of this story is: if you use Slack, don't use it for paging, and make sure your CEO doesn't have access to the paging features.  It will be a disaster.  And if you're running a FOSS project, consider not using Slack, as there are likely many technical people who avoid Slack due to their own experiences with it.
