---
title: "the Alpine release process"
date: "2021-10-22"
---

It's almost Halloween, which means it's almost time for an Alpine release, and all hands are on deck to make sure the process goes smoothly. But what goes into making an Alpine release? What are all the moving parts? Since we are in the process of cutting a new release series, I figured I would write about how it is actually done.

## the beginning of the development cycle

The development cycle for an Alpine release is 6 months long: it begins immediately once the release is branched in `aports.git`: at that point, there is no longer a development freeze, and minor changes start flowing in.

Prior to the beginning of the development cycle, larger changes are proposed as system change proposals, an example of which being the [change proposal introducing Rust to main](https://gitlab.alpinelinux.org/alpine/tsc/-/issues/21) for the Alpine 3.16 development cycle. The largest, most invasive proposals are coordinated by the [Technical Steering Committee](https://gitlab.alpinelinux.org/alpine/tsc), while others may be coordinated by smaller teams, and individual maintainers. Anybody may create a system change proposal and drive it in Alpine, regardless of whether or not they have developer rights in the project.

As these system change proposals are accepted (possibly after a few rounds of revision), the underlying steps needed to implement the change are sequenced into the overall development schedule if needed. Otherwise, they are implemented at the discretion of the contributor driving the change proposal.

## soft freeze

About three weeks before release time, we set up new builders and initiate a mass rebuild of the distribution for the next release. These new builders will continue to follow changes to the `edge` branch until the final release is cut, at which point they will be switched to follow the branch set up for the release.

At this point, the `edge` branch is limited to minor, low risk changes only, unless explicitly granted an exception by the TSC. Efforts are primarily focused on making bug fix changes only, such as resolving failure-to-build-from-source (FTBFS) issues discovered during the rebuild.

## release candidates

The next step before release is to do a few test releases. Release candidates are automatically produced by the builders when a developer updates the `alpine-base` package and tags that commit with an appropriate git tag. These candidates get uploaded to the mirror network and users begin testing them, which usually results in a few bugs being reported which get fixed prior to the final release.

If you are curious, [you can read the code that is run to generate the releases yourself](https://git.alpinelinux.org/aports/tree/scripts/mkimage.sh), it is located in the `aports.git` repository in the `scripts` folder. The main driver of the release generation process is `mkimage.sh`.

## the final release

A few days after each release candidate is cut, the TSC (or a release engineering team delegated by the TSC) evaluates user feedback from testing the new release, and a go/no-go decision is made on making the final release. If the TSC decides the release is not ready, the a new release candidate is made.

Otherwise, if the decision to release is made, then the `aports.git` tree is branched, the new builders are switched to following the new branch, and the final release is cut on that branch. At that point, the `edge` branch is reopened for unrestricted development.

Hopefully, in just a few days, we will have shipped the Alpine 3.15.0 release to the world, with very few release candidates required to do so. So far, the release process has largely gone smoothly, but only time will tell.
