---
title: "Moving my blog to Oracle cloud"
date: "2021-07-18"
---

In my past few blog posts, I have been talking about the [current state of affairs concerning ARM VPS hosting](https://ariadne.space/2021/07/10/its-time-for-arm-to-embrace-traditional-hosting/).  To put my money where my mouth is, I have now migrated my blog to the ARM instances Oracle has to offer, as an actual production use of their cloud.  You might find this surprising, [given the last post](https://ariadne.space/2021/07/14/oracle-cloud-sucks/), but Oracle reached out and explained why their system terminated my original account and we found a solution for that problem.

## What happened, anyway?

Back at the end of May, [Oracle announced that they were offering ARM VPS servers running on Ampere Altra CPUs](https://blogs.oracle.com/cloud-infrastructure/post/arm-based-cloud-computing-is-the-next-big-thing-introducing-arm-on-oracle-cloud-infrastructure).  Accordingly, I was curious, so I signed up for an account on the free tier.  All went well, except that as I was signing up, my now-previous bank declined the initial charge to verify that I had a working credit card.

I was able to sign up anyway, but then a few days later, they charged my card again, which was also declined by my previous bank's overzealous fraud protection.  Then a few weeks later, I attempted to upgrade, and the same thing happened again: first charge was declined, I got a text message and retried, and everything went through.  This weirdness with the card being declined reliably on the first try, however, made Oracle's anti-fraud team anxious, and so they decided to understandably cover their own asses and terminate my account.

I'm going to talk in more depth about my relationship with my previous bank soon, but I want to close my accounts out fully with them before I complain about how awful they are: one does not talk smack about somebody who is holding large sums of your savings, after all.  Needless to say, if you find yourself at a bank being acquired by another bank, run like hell.

Given that Oracle was very proactive in addressing my criticism, and that the issue was caused by something neither myself nor Oracle had any control over (my bank demonstrating very loudly that they needed to be replaced), I decided to give them another chance, and move some of my production services over.

At least, at the moment, since I will no longer be operating my own network as of September, I plan on running my services on a mix of Vultr, Oracle and Linode VMs, as this allows me to avoid Intel CPUs (Oracle have ARM, but also AMD EPYC VMs available, while Vultr and Linode also use AMD EPYC).  I will probably run the more FOSS-centric infrastructure on fosshost's ARM infrastructure, assuming they accept my application anyway.

## Installing Alpine on Oracle Cloud

At present, Alpine images are not offered on Oracle's cloud.  I intend to talk with some of the folks running the service who reached out about getting official Alpine images running in their cloud, as it is a quite decent hosting option.

In the meantime, it is pretty simple to install Alpine.  The first step is to provision an ARM (or x86) instance in their control panel.  You can just use the stock Oracle Linux image, as we will be blasting it away anyway.

Once the image is running, you'll be presented with a control panel like so:

![A control panel for the newly created VPS instance.](/images/Screen-Shot-2021-07-17-at-10.55.35-PM-300x135.png)

The next step is to create an SSH-based serial console.  You will need this to access the Alpine installer.  Scroll down to the resources section and click "Console Connection."  Then click "Create Console Connection":

![Console connections without any created yet.](/images/Screen-Shot-2021-07-17-at-10.59.04-PM-300x58.png)

This will open a modal dialog, where you can specify the SSH key to use.  You'll need to use an RSA key, as this creation wizard doesn't yet recognize Ed25519 keys.  Select "Paste public key" and then paste in your RSA public key, then click "Create console connection" at the bottom of the modal dialog.

The console connection will be created.  Click the menu icon for it, and then click "Copy Serial Console Connection for Linux/Mac."

![Copying the SSH connection command.](/images/Screen-Shot-2021-07-17-at-11.01.55-PM-300x115.png)

Next, open a terminal and paste the command that was copied to your clipboard, and you should be able to access the VPS serial console after dealing with the SSH prompts.

![VPS serial console running Oracle Linux](/images/Screen-Shot-2021-07-17-at-11.04.05-PM-300x180.png)

The next step is to SSH into the machine and download the Alpine installer.  This will just be `ssh opc@1.2.3.4` where 1.2.3.4 is the IP of the instance.  We will want to download the installer ISO to `/run`, which is a ramdisk, and then write it to `/dev/sda` and then sysrq b to reboot.  Here's what that looks like:

![Preparing the Alpine installer](/images/Screen-Shot-2021-07-17-at-11.09.40-PM-300x180.png)

If you monitor your serial console window, you'll find that you've been dropped into the Alpine installer ISO.

![Alpine installer shell](/images/Screen-Shot-2021-07-17-at-11.11.39-PM-300x180.png)

From here, you can run `setup-alpine` and follow the directions as usual.  You will want to overwrite the boot media, so answer yes when it asks.

![Installing Alpine](/images/Screen-Shot-2021-07-17-at-11.15.02-PM-300x179.png)

At this point, you can reboot, and it will dump you into your new Alpine image.  You might want to set up `cloud-init`, or whatever, but that's not important to cover here.

## Future plans

At the moment, the plan is to see how things perform, and if they perform well, migrate more services over.  I might also create OCIs with `cloud-init` enabled for other users of Alpine on Oracle cloud.

Stay tuned!
