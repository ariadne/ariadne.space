---
title: "NFTs: A Scam that Artists Should Avoid"
date: "2021-03-21"
coverImage: "Screenshot_2021-03-21-NFTMagic-NFTs-on-Ardor-fully-stored-on-blockchain-and-low-fees.png"
---

Non-fungible tokens (NFTs) are the latest craze being pitched toward the artistic communities.  But, they are ultimately a meaningless token which fails to accomplish any of the things artists are looking for in an NFT-based solution.

Let me explain...

## So, What are NFTs?

Non-fungible tokens are a form of smart contracts (program) which runs on a decentralized finance platform.

They are considered "non-fungible" because they reference a specific asset, while a fungible token would represent an asset that is not specific.  An example of a non-fungible token in the physical world would be the title to your car or house, while a fungible token would be currency.

These smart contracts could, if correctly implemented, represent title to a physical asset, but implementation of an effective NFT regime would require substantive changes to the contract law in order to be enforceable in the current legal system.

## How do NFTs apply to artwork?

Well, the simple answer is that they don't.  You might hear differently from companies that are selling NFT technology to you, as an artist or art collector, but there is no actual mechanism to enforce the terms of the smart contract in a court and there is no actual mechanism to enforce that the guarantees of the smart contract itself cannot be bypassed.

NFT platforms like ArtMagic try to make it look like it is possible to enforce restrictions on your intellectual property using NFTs.  For example, [this NFT is listed as being limited to 1000 copies](https://nftmagic.io/view?asset=12588085591129036864):

![](/images/Screenshot_2021-03-21-NFTMagic-300x151.png)

However, there is no mechanism to actually enforce this.  It is possible that only 1000 instances of the NFT can be _sold_, but this does not restrict the actual number of copies to 1000.  To demonstrate this, I have made a copy of this artwork and reproduced it on my own website, which exists outside of the world where the NFT has any enforcement mechanism.

![](/images/12588085591129036864-169x300.jpeg)

As you can see, there are now at least 1001 available copies of this artwork.  Except you can download that one for free.

All of these platforms have various ways of finding the master copy of the artwork and thus enabling you to make your own copy.  I am planning on writing a tool soon to allow anyone to download their own personal copy of any NFT.  I'll write about the internals of the smart contracts and how to get the goods later.

## Well, what about archival?

Some companies, like NFTMagic claim that your artwork is stored on the blockchain forever.  In practice, this is a very bold claim, because it requires that:

- Data is never evicted from the blockchain in order to make room for new data.
- The blockchain will continue to exist forever.
- Appending large amounts of data to the blockchain will always be possible.

Lets look into this for NFTMagic, since they make such a bold claim.

NFTMagic runs on the [Ardor blockchain platform](https://www.jelurida.com/ardor), which is [written in Java](https://github.com/NXTARDOR/ARDOR).  This is already somewhat concerning because Java is not a very efficient language for writing this kind of software in.  But how is the blockchain stored to disk?

For that purpose, the [Ardor software uses H2](https://github.com/NXTARDOR/ARDOR/blob/master/src/java/nxt/db/BasicDb.java#L21).  H2 is basically an SQLite clone for Java.  So far, this is not very confidence inspiring.  By comparison, Bitcoin and Ethereum use LevelDB, which is far more suited for this task (maintaining a content-addressable append-only log) than an SQL database of any kind.

How is the data actually archived to the blockchain?  In the case of Ardor, it works by having some participants act as _archival nodes_.  These archival nodes maintain a full copy of the asset blockchain -- you either get everything or you get nothing.  By comparison, other archival systems, like IPFS, allow you to specify what buckets you would like to duplicate.

How does data get to the archival nodes?  It is split into 42KB chunks and committed to the chain with a manifest.  Those 42KB chunks and manifest are then stored in the SQL database.

Some proponents of the NFTMagic platform claim that this design ensures that there will be at least one archival node available at all times.  But I am skeptical, because the inefficient data storage mechanism in combination with the economics of being an archival node make this unlikely to be sustainable in the long term.  If things play out the way I think they will, there will be a point in the future where zero archival nodes exist.

However, there is a larger problem with this design: if some archival nodes choose to be evil, they can effectively deny the rightful NFT owner's ability to download the content she has purchased.  I believe implementing an evil node on the Ardor network would actually not be terribly difficult to do.  A cursory examination of the `getMessage` API does not seem to provide any protections against evil archival nodes.  At the present size of the network, a nation state would have more than sufficient resources to pull off this kind of attack.

## Conclusion

In closing, I hope that by looking at the NFTMagic platform and it's underlying technology (Ardor), I have effectively conveyed that these NFT platforms are not terribly useful to artists for more than a glorified "certificate of authenticity" solution.  You could, incidentally, do a "certificate of authenticity" by simply writing one and signing it with something like Docusign.  That would be more likely to be enforceable in a court too.

I also hope I have demonstrated that NFTs by design cannot restrict anyone from making a copy.  Be careful when evaluating the claims made by NFT vendors.  When in doubt, check your fingers and check your wallet, because they are most assuredly taking you for a ride.
